package org.catools.common.testng.annotatoins;

import org.catools.common.exception.CRuntimeException;
import org.catools.common.tests.CTest;
import org.testng.annotations.Test;

public class CLabelTest extends CTest {

  @Test
  @IANY1
  @RANY1
  public void ignoreByOneLabel() {
    throw new CRuntimeException("Should skip by one label");
  }

  @Test
  @IANY1
  @IANY2
  @RANY1
  public void ignoreByTwoLabel() {
    throw new CRuntimeException("Should skip by two label");
  }

  @Test
  @IANY1
  @IANY2
  @IANY3
  @RANY1
  public void ignoreByAllAnyLabel() {
    throw new CRuntimeException("Should skip by all ANY label");
  }

  @Test
  @RALL1
  public void ignoreByOneAllLabel() {
    throw new CRuntimeException("Should skip by only one ALL label");
  }

  @Test
  @IALL1
  @IALL2
  @IALL3
  public void ignoreByAllLabel() {
    throw new CRuntimeException("Should skip by ALL label");
  }

  @Test
  @RALL1
  @RALL2
  public void ignoreByTwoAllLabel() {
    throw new CRuntimeException("Should skip by two ALL label");
  }

  @Test
  @RALL1
  @RALL2
  @RALL3
  public void shouldRunByAllLabel() {}

  @Test
  @RALL1
  @RANY1
  @RALL3
  public void shouldRunByAnyLabel() {}

  @Test(dependsOnMethods = {"shouldRunByAllLabel", "shouldRunByAnyLabel"})
  @RANY1
  public void testLabels() {}
}
