package org.catools.common.config;

public class CCoreConfigs extends CConfigs {
  public static boolean isCleanupModeOn() {
    return getConfigs().getBooleanOrElse(Configs.CORE_CLEANUP_MODE, false);
  }

  public static boolean isDataSetupModeOn() {
    return getConfigs().getBooleanOrElse(Configs.CORE_DATA_SETUP_ENABLE, false);
  }

  public static boolean isLocalStorageEnable() {
    return getConfigs().getBooleanOrElse(Configs.CORE_LOCAL_STORAGE_ENABLE, true);
  }

  public static boolean isProductionModeOn() {
    return getConfigs().getBooleanOrElse(Configs.CORE_PRODUCTION_MODE, false);
  }

  public static boolean isReleaseModeOn() {
    return getConfigs().getBooleanOrElse(Configs.CORE_RELEASE_MODE, false);
  }

  private enum Configs {
    CORE_DATA_SETUP_ENABLE,
    CORE_CLEANUP_MODE,
    CORE_LOCAL_STORAGE_ENABLE,
    CORE_RELEASE_MODE,
    CORE_PRODUCTION_MODE
  }
}
