package org.catools.common.config;

import org.catools.common.collections.CList;
import org.catools.common.logger.CLoggerConfigs;
import org.catools.common.testng.CTestNGConfigs;
import org.catools.common.utils.CConfigUtil;
import org.catools.common.utils.CConsoleUtil;
import org.catools.common.utils.CStringUtil;

import java.util.stream.Collectors;

public class CCliConfigsLoader {

  public static void init(Class testClassLoader, boolean maskSensitiveInfo, String[] args) {
    System.setProperty(
        CConfigsLoader.CONFIGS_TO_LOAD.getName(),
        CConfigUtil.getValueOrDefault(
            CConfigsLoader.CONFIGS_TO_LOAD.getName(),
            CConfigUtil.readArgs(args).get("CONFIGS_TO_LOAD")));

    // We read command line here in case if we send CONFIGS_TO_LOAD by command line
    loadArgsToConfigs(args);

    CTestNGConfigs.setBaseTestClassLoader(testClassLoader.getName());
    CLoggerConfigs.setMaskSensitiveData(maskSensitiveInfo);

    for (CConfigInfo property : getRequiredToPromptProperties()) {
      property.setValue(
          CConsoleUtil.prompt(
              property.getDescription(), property.getDefaultValue(), property.isSensitive()));
    }

    // We Should reload settings from configuration parameters to make sure that command lines
    // parameter has higher priority
    loadArgsToConfigs(args);
  }

  private static CList<CConfigInfo> getRequiredToPromptProperties() {
    return CList.of(
        CConfigs.getConfigs().stream()
            .filter(c -> c.isRequired() && !c.isDefined() && CStringUtil.isBlank(c.getValue()))
            .collect(Collectors.toList()));
  }

  private static void loadArgsToConfigs(String[] args) {
    CConfigUtil.readArgs(args)
        .forEach(
            (k, v) -> {
              try {
                CConfigs.getByName(k).setValue(v);
              } catch (CConfigNotDefinedException ex) {
                System.setProperty(k, v);
              }
            });
  }
}
