package org.catools.common.testng;

import org.catools.common.collections.CList;
import org.catools.common.collections.CSet;
import org.catools.common.config.CConfigs;
import org.catools.common.utils.CConfigUtil;
import org.catools.common.utils.CStringUtil;
import org.testng.ITestNGListener;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public class CTestNGConfigs extends CConfigs {
  private static final AtomicInteger SUITE_RUN_COUNTER = new AtomicInteger(1);

  public static Class getBaseClassLoader() {
    try {
      return Class.forName(getConfigs().getString(Configs.TESTNG_BASE_TEST_CLASS_LOADER));
    } catch (ClassNotFoundException e) {
      throw new RuntimeException(e);
    }
  }

  public static Set<ITestNGListener> getListeners() {
    final Set<ITestNGListener> listeners = new HashSet<>();
    String testNgListeners = getConfigs().getStringOrElse(Configs.TESTNG_LISTENERS, null);
    if (testNgListeners != null) {
      for (String listener : getConfigs().getStrings(Configs.TESTNG_LISTENERS, ",")) {
        try {
          listeners.add((ITestNGListener) Class.forName(listener).getConstructor().newInstance());
        } catch (Throwable t) {
          System.out.println(
              "Could not convert Configs.TESTNG_LISTENERS parameter " + listener + " to classes.");
          System.out.println(t.toString());
        }
      }
    }
    return listeners;
  }

  public static int getSeverityLevel() {
    return getConfigs().getIntegerOrElse(Configs.TESTNG_RUN_SEVERITY_LEVEL, -1);
  }

  public static int getRegressionDepth() {
    return getConfigs().getIntegerOrElse(Configs.TESTNG_RUN_REGRESSION_DEPTH, -1);
  }

  public static CList<String> getAnnotationsToIgnoreTestIfAnyMatch() {
    return CList.of(
        getConfigs()
            .getStringsOrElse(Configs.TESTNG_IGNORE_TEST_WITH_ANY_ANNOTATION, ",", CList.of()));
  }

  public static CList<String> getAnnotationsToIgnoreTestIfAllMatch() {
    return CList.of(
        getConfigs()
            .getStringsOrElse(Configs.TESTNG_IGNORE_TEST_WITH_ALL_ANNOTATION, ",", CList.of()));
  }

  public static CList<String> getAnnotationsToRunTestIfAllMatch() {
    return CList.of(
        getConfigs()
            .getStringsOrElse(Configs.TESTNG_RUN_TEST_WITH_ALL_ANNOTATIONS, ",", CList.of()));
  }

  public static CList<String> getAnnotationsToRunTestIfAnyMatch() {
    return CList.of(
        getConfigs()
            .getStringsOrElse(Configs.TESTNG_RUN_TEST_WITH_ANY_ANNOTATIONS, ",", CList.of()));
  }

  public static int getTestRetryCount() {
    return getConfigs().getIntegerOrElse(Configs.TESTNG_TEST_RETRY_COUNT, 0);
  }

  public static int getSuiteRetryCount() {
    return getConfigs().getIntegerOrElse(Configs.TESTNG_SUITE_RETRY_COUNT, 0);
  }

  public static int getSuiteRunCounter() {
    return SUITE_RUN_COUNTER.get();
  }

  public static CSet<String> getTestPackages() {
    return new CSet<>(getConfigs().getStrings(Configs.TESTNG_TEST_PACKAGES, ","));
  }

  public static int getThreadCount() {
    return getConfigs().getIntegerOrElse(Configs.TESTNG_THREAD_COUNT, 1);
  }

  public static Integer incrementSuiteRun() {
    int suiteRun = SUITE_RUN_COUNTER.incrementAndGet();
    CConfigUtil.setRunName(isLastSuiteRun() ? CStringUtil.EMPTY : "run_" + suiteRun);
    return suiteRun;
  }

  public static boolean isFirstSuiteRun() {
    return getSuiteRunCounter() == 1;
  }

  public static boolean isLastSuiteRun() {
    return getSuiteRunCounter() >= getSuiteRetryCount() + 1;
  }

  public static void setBaseTestClassLoader(String className) {
    getConfigs().getByName(Configs.TESTNG_BASE_TEST_CLASS_LOADER).setValue(className);
  }

  public static boolean skipClassWithAwaitingTest() {
    return getConfigs().getBooleanOrElse(Configs.TESTNG_SKIP_CLASS_WITH_AWAITING_TEST, false);
  }

  public static boolean skipClassWithIgnoredTest() {
    return getConfigs().getBooleanOrElse(Configs.TESTNG_SKIP_CLASS_WITH_IGNORED_TEST, false);
  }

  public static String getTestNgResultName() {
    return getConfigs().getStringOrElse(Configs.TESTNG_RESULT_XML_NAME, "cat-testng-results");
  }

  private enum Configs {
    TESTNG_TEST_PACKAGES,
    TESTNG_THREAD_COUNT,
    TESTNG_LISTENERS,
    TESTNG_BEFORE_INIT_LISTENERS,
    TESTNG_TEST_RETRY_COUNT,
    TESTNG_SUITE_RETRY_COUNT,
    TESTNG_BASE_TEST_CLASS_LOADER,
    TESTNG_SKIP_CLASS_WITH_AWAITING_TEST,
    TESTNG_SKIP_CLASS_WITH_IGNORED_TEST,
    TESTNG_RUN_SEVERITY_LEVEL,
    TESTNG_RUN_REGRESSION_DEPTH,
    TESTNG_IGNORE_TEST_WITH_ANY_ANNOTATION,
    TESTNG_IGNORE_TEST_WITH_ALL_ANNOTATION,
    TESTNG_RUN_TEST_WITH_ALL_ANNOTATIONS,
    TESTNG_RUN_TEST_WITH_ANY_ANNOTATIONS,
    TESTNG_RESULT_XML_NAME
  }
}
