package org.catools.web.axe.entities;

import lombok.Data;
import org.catools.common.collections.CSet;

@Data
public class CAxeViolations extends CSet<CAxeNode> {}
