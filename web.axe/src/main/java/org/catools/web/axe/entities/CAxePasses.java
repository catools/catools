package org.catools.web.axe.entities;

import lombok.Data;
import org.catools.common.collections.CSet;

@Data
public class CAxePasses extends CSet<CAxeNode> {}
