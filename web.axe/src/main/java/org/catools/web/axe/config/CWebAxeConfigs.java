package org.catools.web.axe.config;

import org.catools.common.utils.CConfigUtil;

public class CWebAxeConfigs {
  public static boolean isAxeAnalyserEnable() {
    return CConfigUtil.getBooleanOrElse(Configs.WEB_AXE_ANALYSER_ENABLE, false);
  }

  public enum Configs {
    WEB_AXE_ANALYSER_ENABLE
  }
}
