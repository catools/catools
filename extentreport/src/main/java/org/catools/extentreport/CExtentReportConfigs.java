package org.catools.extentreport;

import org.catools.common.config.CConfigs;

public class CExtentReportConfigs extends CConfigs {

  public static String getExtentReportFileName() {
    return getConfigs().getStringOrElse(Configs.EXTENT_REPORT_FILE_NAME, "ExtentReport");
  }

  public static String getExtentReportName() {
    return getConfigs().getStringOrElse(Configs.EXTENT_REPORT_NAME, "Extent Report");
  }

  public static boolean isEnable() {
    return getConfigs().getBooleanOrElse(Configs.EXTENT_REPORT_ENABLE, true);
  }

  private enum Configs {
    EXTENT_REPORT_ENABLE,
    EXTENT_REPORT_NAME,
    EXTENT_REPORT_FILE_NAME
  }
}
