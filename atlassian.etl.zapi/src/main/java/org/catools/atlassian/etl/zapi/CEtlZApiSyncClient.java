package org.catools.atlassian.etl.zapi;

import lombok.experimental.UtilityClass;
import org.catools.atlassian.etl.zapi.translators.CEtlZApiTranslator;
import org.catools.atlassian.zapi.CZApiClient;
import org.catools.atlassian.zapi.model.*;
import org.catools.common.collections.CList;
import org.catools.common.collections.CSet;
import org.catools.common.concurrent.CParallelRunner;
import org.catools.common.date.CDate;
import org.catools.common.exception.CRuntimeException;
import org.catools.common.utils.CStringUtil;
import org.catools.tms.etl.cache.CEtlCacheManager;
import org.catools.tms.etl.dao.CEtlCycleDao;
import org.catools.tms.etl.dao.CEtlExecutionDao;
import org.catools.tms.etl.dao.CEtlLastSyncDao;
import org.catools.tms.etl.model.CEtlExecution;
import org.catools.tms.etl.model.CEtlExecutionStatus;
import org.catools.tms.etl.model.CEtlExecutions;
import org.slf4j.Logger;

import java.util.Date;

@UtilityClass
public class CEtlZApiSyncClient {
  private static final String ZAPI = "ZAPI";
  private static final String UNSCHEDULED = "Unscheduled";

  public static void syncZephyr(
      Logger logger,
      CSet<String> projectNamesToSync,
      int parallelInputCount,
      int parallelOutputCount) {
    CZApiProjects projects = CZApiClient.Project.getProjects();
    CZApiClient.ExecutionStatus.getExecutionStatus()
        .forEach(
            ex ->
                CEtlCacheManager.readExecutionStatus(
                    logger, new CEtlExecutionStatus(String.valueOf(ex.getId()), ex.getName())));

    Date syncStartTime = CDate.now();
    for (CZApiProject project : projects.getAll(p -> projectNamesToSync.contains(p.getName()))) {
      CZApiVersions projectVersions =
          CZApiClient.Version.getProjectVersions(project).getAllVersions();
      // remove Unscheduled cycles
      projectVersions.removeIf(v -> CStringUtil.containsIgnoreCase(UNSCHEDULED, v.getName()));

      // If there were full sync then we read diff and update versions to be update to read only
      // changed ones.
      // We have to read information again based on changed versions as there is no indicator In Erp
      // level for Cycle
      // to ensure that we are working with. We can add additional fields like internal id or
      // something like that but
      // depend on Test Data Management this option will be very so for now we just read records
      // again.
      // Assuming tht Erp usually run at least once a day, the number of diff should not be that
      // much to be worry about.
      // In future having more TDM we can make a better decision.
      Date projectLastZApiSync =
          CEtlLastSyncDao.getProjectLastSync(logger, ZAPI, project.getName());
      if (projectLastZApiSync != null) {
        CZApiExecutions executions =
            CZApiClient.Search.getExecutions(
                String.format("project='%s'", project.getName()),
                projectLastZApiSync,
                parallelInputCount,
                parallelOutputCount,
                null);
        CSet<String> versionsToUpdate = executions.mapToSet(CZApiExecution::getVersionName);
        projectVersions.removeIf(v -> !versionsToUpdate.contains(v.getName()));
      }

      for (CZApiVersion version : projectVersions) {
        CZApiCycles cycles =
            CZApiClient.Cycle.getAllCycle(
                new CZApiProject().setId(project.getId()).setName(project.getName()),
                new CZApiVersion().setId(version.getId()).setName(version.getName()));
        if (cycles.isNotEmpty()) {
          CEtlCycleDao.mergeCycles(logger, CEtlZApiTranslator.translateCycles(logger, cycles));
          addExecutions(
              logger,
              project.getName(),
              version.getName(),
              parallelInputCount,
              parallelOutputCount);
        }
      }
      CEtlLastSyncDao.updateProjectLastSync(logger, ZAPI, project.getName(), syncStartTime);
    }
  }

  private static void addExecutions(
      Logger logger,
      String projectName,
      String versionName,
      int parallelInputCount,
      int parallelOutputCount) {
    Date lastSync = CEtlLastSyncDao.getExecutionLastSync(logger, ZAPI, projectName, versionName);
    String zql = String.format("project='%s' AND fixVersion = \"%s\"", projectName, versionName);

    Date syncStartTime = CDate.now();
    CEtlExecutions executions = new CEtlExecutions();
    CZApiClient.Search.getExecutions(
        zql,
        lastSync,
        parallelInputCount,
        parallelOutputCount,
        zApiExecutions -> {
          if (zApiExecutions.isNotEmpty()) {
            // I surprised when I notice that in particular moment, search can return items which
            // are not related to
            // project or version we search for, so we need to clean them up before continuing.
            // I have no clue why and could not found any information online about this.
            zApiExecutions.removeIf(
                e ->
                    !projectName.equalsIgnoreCase(e.getProjectName())
                        || !versionName.equalsIgnoreCase(e.getVersionName()));

            if (zApiExecutions.isNotEmpty()) {
              CEtlExecutions translatedExecutions =
                  CEtlZApiTranslator.translateExecutions(logger, zApiExecutions);
              logger.info(
                  "{} execution records translated to {} etl execution records",
                  zApiExecutions.size(),
                  translatedExecutions.size());

              synchronized (executions) {
                executions.addAll(translatedExecutions);
                logger.info("{} executions translated.", executions.size());
              }
            }
          }
        });

    logger.info("Total number of translate executions are {}", executions.size());

    if (executions.isNotEmpty()) {
      try {
        int partitionSize =
            executions.size() < parallelOutputCount
                ? executions.size()
                : executions.size() / parallelOutputCount;
        CList<CList<CEtlExecution>> partitions = executions.partition(partitionSize);

        new CParallelRunner<>(
                "Merge Executions",
                Math.min(partitions.size(), parallelOutputCount),
                () -> {
                  while (true) {
                    CEtlExecutions executionsPartition = null;
                    synchronized (partitions) {
                      if (partitions.isNotEmpty()) {
                        executionsPartition = new CEtlExecutions(partitions.remove(0));
                      }
                    }
                    if (executionsPartition == null) {
                      break;
                    }
                    CEtlExecutionDao.mergeExecutions(logger, executionsPartition);
                  }
                  return true;
                })
            .invokeAll();
      } catch (Throwable t) {
        throw new CRuntimeException("Failed to persist test execution.", t);
      }
    }

    CEtlLastSyncDao.updateExecutionLastSync(logger, ZAPI, projectName, versionName, syncStartTime);
  }
}
