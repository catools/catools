package org.catools.atlassian.etl.zapi.translators;

import com.atlassian.jira.rest.client.api.domain.User;
import com.atlassian.jira.rest.client.api.domain.Version;
import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.StringUtils;
import org.catools.atlassian.etl.jira.translators.CEtlJiraTranslator;
import org.catools.atlassian.jira.client.CJiraClient;
import org.catools.atlassian.zapi.CZApiClient;
import org.catools.atlassian.zapi.model.CZApiCycle;
import org.catools.atlassian.zapi.model.CZApiCycles;
import org.catools.atlassian.zapi.model.CZApiExecution;
import org.catools.atlassian.zapi.model.CZApiExecutions;
import org.catools.common.exception.CRuntimeException;
import org.catools.tms.etl.dao.*;
import org.catools.tms.etl.model.*;
import org.slf4j.Logger;

@UtilityClass
public class CEtlZApiTranslator {
  public static CEtlCycles translateCycles(Logger logger, CZApiCycles cycles) {
    return new CEtlCycles(cycles.mapToSet(c -> translateCycle(logger, c)));
  }

  public static CEtlCycle translateCycle(Logger logger, CZApiCycle cycle) {
    try {
      CEtlProject project =
          CEtlProjectDao.getProjects(logger).getByName(cycle.getProject().getName());
      CEtlVersion version =
          CEtlVersionDao.getVersion(logger, project, cycle.getVersion().getName());
      if (version == null) {
        CEtlVersions versions = new CEtlVersions();
        for (Version projectVersion : CJiraClient.getProjectVersions(project.getId())) {
          versions.add(CEtlJiraTranslator.translateVersion(project, projectVersion));
        }
        CEtlVersionDao.mergeVersions(logger, versions);
        version = CEtlVersionDao.getVersion(logger, project, cycle.getVersion().getName());
      }
      return new CEtlCycle(
          String.valueOf(cycle.getId()),
          cycle.getName(),
          cycle.getDescription(),
          version,
          cycle.getEnvironment(),
          cycle.getEndDate(),
          cycle.getBuild(),
          cycle.getStartDate());
    } catch (Throwable t) {
      throw new CRuntimeException("Failed to translate cycle.", t);
    }
  }

  public static CEtlExecutions translateExecutions(Logger logger, CZApiExecutions executions) {
    return new CEtlExecutions(executions.mapToSet(i -> translateExecution(logger, i)));
  }

  public static CEtlExecution translateExecution(Logger logger, CZApiExecution execution) {
    CEtlExecutionStatus status =
        CEtlExecutionStatusDao.getStatusByName(logger, execution.getExecutionStatus());
    CEtlItem item = CEtlItemDao.getItemById(logger, execution.getIssueKey());
    CEtlCycle cycle = CEtlCycleDao.getCycleById(logger, String.valueOf(execution.getCycleId()));
    CEtlUser executor = getExecutor(logger, execution);

    if (cycle == null && execution.getCycleId() != null) {
      cycle = translateCycle(logger, CZApiClient.Cycle.getCycleById(execution.getCycleId()));
      CEtlCycleDao.mergeCycle(logger, cycle);
    }

    return new CEtlExecution(
        String.valueOf(execution.getId()),
        item,
        cycle,
        execution.getCreatedOn(),
        execution.getExecutedOn(),
        executor,
        status,
        execution.getExecutionDefectCount(),
        execution.getStepDefectCount(),
        execution.getTotalDefectCount(),
        execution.getComment());
  }

  protected static CEtlUser getExecutor(Logger logger, CZApiExecution execution) {
    if (StringUtils.isNotBlank(execution.getExecutedByUserName())) {
      CEtlUser executor = CEtlUserDao.getUserByName(logger, execution.getExecutedByUserName());
      if (executor == null) {
        addNewUser(logger, execution);
        executor = CEtlUserDao.getUserByName(logger, execution.getExecutedByUserName());
      }
      return executor;
    }
    return null;
  }

  // As far as we might persist use in this method, to have a safe multithreaded processing, we need
  // this method to be synchronized
  private static synchronized void addNewUser(Logger logger, CZApiExecution execution) {
    User user = CJiraClient.getUser(execution.getExecutedByUserName());
    CEtlUserDao.mergeUser(
        logger, new CEtlUser(user.getName(), user.getEmailAddress(), user.getDisplayName()));
  }
}
