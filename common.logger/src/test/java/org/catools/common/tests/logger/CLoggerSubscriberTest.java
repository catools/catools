package org.catools.common.tests.logger;

import org.apache.logging.log4j.Level;
import org.catools.common.collections.CHashMap;
import org.catools.common.collections.interfaces.CMap;
import org.catools.common.logger.CLoggerSubscriber;
import org.catools.common.tests.CBaseUnitTest;
import org.testng.annotations.Test;

public class CLoggerSubscriberTest extends CBaseUnitTest {

  @Test
  public void testSubscribe() {
    CMap<String, Level> events = new CHashMap<>();
    CLoggerSubscriber.subscribe("A", (level, msg) -> events.put(msg, level));
    logger.trace("ERROR");
    logger.info("ERROR");
    logger.error("ERROR");

    CLoggerSubscriber.unSubscribe("A");
    logger.error("ERROR2");

    events.verifyContains(this, "ERROR", Level.ERROR);
    events.verifyNotContains(this, "ERROR2", Level.ERROR);
  }
}
