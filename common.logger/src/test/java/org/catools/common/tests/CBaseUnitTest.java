package org.catools.common.tests;

import org.catools.common.extensions.verify.CVerificationInfo;
import org.catools.common.extensions.verify.CVerificationQueue;
import org.catools.common.extensions.verify.CVerify;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CBaseUnitTest implements CVerificationQueue {

  protected Logger logger = LoggerFactory.getLogger(this.getClass());
  protected CVerify verify = new CVerify(logger);

  @Override
  public Logger getLogger() {
    return logger;
  }

  @Override
  public CVerify queue(CVerificationInfo verificationInfo) {
    return verify.queue(verificationInfo);
  }
}
