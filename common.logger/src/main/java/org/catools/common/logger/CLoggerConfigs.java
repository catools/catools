package org.catools.common.logger;

import org.catools.common.utils.CAnsiUtil;
import org.catools.common.utils.CConfigUtil;
import org.catools.common.utils.CFileUtil;

import static org.catools.common.configs.CPathConfigs.getOutputChildFolder;

public class CLoggerConfigs {
  public static boolean logColoredOutput() {
    return isOutputSupportAnsi() && CConfigUtil.getBooleanOrElse(Configs.LOGGER_LOG_COLORED, true);
  }

  public static boolean isOutputSupportAnsi() {
    return CAnsiUtil.isConsoleAvailable() || CAnsiUtil.isSystemOutSupportAnsi() || isXterm();
  }

  public static boolean isXterm() {
    String term = System.getenv("TERM");
    return term != null && term.toLowerCase().startsWith("xterm");
  }

  public static String getLogFolderPath() {
    return CFileUtil.getCanonicalPath(getOutputChildFolder("log"));
  }

  public static void setMaskSensitiveData(boolean flag) {
    System.setProperty(Configs.LOGGER_MASK_SENSITIVE_DATA.name(), Boolean.toString(flag));
  }

  public static boolean maskSensitiveData() {
    return CConfigUtil.getBooleanOrElse(Configs.LOGGER_MASK_SENSITIVE_DATA, true);
  }

  private enum Configs {
    LOGGER_MASK_SENSITIVE_DATA,
    LOGGER_LOG_COLORED
  }
}
