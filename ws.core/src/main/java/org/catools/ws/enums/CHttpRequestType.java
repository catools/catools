package org.catools.ws.enums;

public enum CHttpRequestType {
  GET,
  PUT,
  POST,
  DELETE,
  PATCH
}
