package org.catools.web.drivers.config;

import org.catools.common.collections.CList;
import org.catools.common.utils.CConfigUtil;
import org.openqa.selenium.PageLoadStrategy;

public class CFireFoxConfigs {

  public static String getDriverPath() {
    return CConfigUtil.getStringOrElse(Configs.WEB_FIREFOX_DRIVER_PATH, "");
  }

  public static String getBinaryPath() {
    return CConfigUtil.getStringOrElse(Configs.WEB_FIREFOX_BINARY_PATH, "");
  }

  public static CList<String> getDefaultArguments() {
    return CList.of(
        CConfigUtil.getStringsOrElse(Configs.WEB_FIREFOX_DEFAULT_ARGUMENTS, ",", CList.of()));
  }

  public static PageLoadStrategy getPageLoadStrategy() {
    return PageLoadStrategy.valueOf(
        CConfigUtil.getStringOrElse(
            Configs.WEB_FIREFOX_PAGE_LOAD_STRATEGY, PageLoadStrategy.NORMAL.name()));
  }

  public static boolean isInHeadLessMode() {
    return CConfigUtil.getBooleanOrElse(Configs.WEB_FIREFOX_HEADLESS_ENABLE, false);
  }

  public static CList<String> getHeadLessArguments() {
    return CList.of(
        CConfigUtil.getStringsOrElse(Configs.WEB_FIREFOX_HEADLESS_ARGUMENTS, ",", CList.of()));
  }

  public enum Configs {
    WEB_FIREFOX_DRIVER_PATH,
    WEB_FIREFOX_BINARY_PATH,
    WEB_FIREFOX_DEFAULT_ARGUMENTS,
    WEB_FIREFOX_PAGE_LOAD_STRATEGY,
    WEB_FIREFOX_HEADLESS_ENABLE,
    WEB_FIREFOX_HEADLESS_ARGUMENTS;
  }
}
