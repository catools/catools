package org.catools.web.drivers.config;

import org.catools.common.collections.CList;
import org.catools.common.utils.CConfigUtil;
import org.openqa.selenium.PageLoadStrategy;

public class CEdgeConfigs {


    public static String getDriverPath() {
        return CConfigUtil.getStringOrElse(CEdgeConfigs.Configs.WEB_EDGE_DRIVER_PATH, "");
    }

    public static String getBinaryPath() {
        return CConfigUtil.getStringOrElse(CEdgeConfigs.Configs.WEB_EDGE_BINARY_PATH, "");
    }

    public static CList<String> getDefaultArguments() {
        return CList.of(
                CConfigUtil.getStringsOrElse(CEdgeConfigs.Configs.WEB_EDGE_DEFAULT_ARGUMENTS, ",", CList.of()));
    }

    public static PageLoadStrategy getPageLoadStrategy() {
        return PageLoadStrategy.valueOf(
                CConfigUtil.getStringOrElse(
                        CEdgeConfigs.Configs.WEB_EDGE_PAGE_LOAD_STRATEGY, PageLoadStrategy.NORMAL.name()));
    }

    public static boolean isInHeadLessMode() {
        return CConfigUtil.getBooleanOrElse(CEdgeConfigs.Configs.WEB_EDGE_HEADLESS_ENABLE, false);
    }

    public static CList<String> getHeadLessArguments() {
        return CList.of(
                CConfigUtil.getStringsOrElse(CEdgeConfigs.Configs.WEB_EDGE_HEADLESS_ARGUMENTS, ",", CList.of()));
    }

    public enum Configs {
        WEB_EDGE_DRIVER_PATH,
        WEB_EDGE_BINARY_PATH,
        WEB_EDGE_DEFAULT_ARGUMENTS,
        WEB_EDGE_PAGE_LOAD_STRATEGY,
        WEB_EDGE_HEADLESS_ENABLE,
        WEB_EDGE_HEADLESS_ARGUMENTS;
    }
}
