package org.catools.web.drivers.providers;

import io.github.bonigarcia.wdm.WebDriverManager;
import lombok.SneakyThrows;
import org.catools.common.collections.CHashMap;
import org.catools.common.collections.CList;
import org.catools.common.collections.interfaces.CMap;
import org.catools.common.configs.CPathConfigs;
import org.catools.common.io.CFile;
import org.catools.common.utils.CStringUtil;
import org.catools.web.config.CGridConfigs;
import org.catools.web.drivers.CDriverProvider;
import org.catools.web.drivers.config.CChromeConfigs;
import org.catools.web.drivers.config.CWebDriverManagerConfigs;
import org.catools.web.enums.CBrowser;
import org.catools.web.listeners.CDriverListener;
import org.openqa.selenium.PageLoadStrategy;
import io.testproject.sdk.drivers.web.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.catools.web.config.CGridConfigs.getHubURL;
import static org.catools.web.config.CGridConfigs.isUseRemoteDriver;

public class CTestProjectChromeDriverProvider implements CDriverProvider {
  private static final Object LOCK = new Object();
  private CMap<String, Object> prefs = new CHashMap<>();
  private CList<CMap<String, Object>> plugins = new CList<>();
  private ChromeOptions options = new ChromeOptions();

  static {
    if (CWebDriverManagerConfigs.isEnabled()) {
      WebDriverManager.chromedriver().setup();
    }
  }

  public CTestProjectChromeDriverProvider() {
    if (CStringUtil.isNotBlank(CChromeConfigs.getBinaryPath())) {
      setBinary(CChromeConfigs.getBinaryPath());
    }
    addArguments(CChromeConfigs.getDefaultArguments());
    addPluginsToEnable(CChromeConfigs.getPluginsToEnable());
    addPluginsToDisable(CChromeConfigs.getPluginsToDisable());
    setPageLoadStrategy(CChromeConfigs.getPageLoadStrategy());
    setDownloadFolder(CPathConfigs.getTmpDownloadFolder());
    setOpenPdfInNewTab(true);

    prefs.put("plugins.plugins_list", plugins);

    if (CStringUtil.isNotBlank(CChromeConfigs.getChromeMobileEmulationDeviceName())) {
      setDeviceEmulation(CChromeConfigs.getChromeMobileEmulationDeviceName());
    }

    setHeadless(CChromeConfigs.isInHeadLessMode());
  }

  @SneakyThrows
  @Override
  public RemoteWebDriver build(Logger logger, List<CDriverListener> listeners) {
    buildChromeOptions();
    if (listeners != null) {
      listeners.forEach(b -> b.beforeInit(logger, options));
    }

    // it happens that in high race condition we might have more than
    // multiple driver with one profile which causes different issues
    // specially with chrome
    RemoteWebDriver webDriver;
    synchronized (LOCK) {
      webDriver =
          isUseRemoteDriver()
              ? new RemoteWebDriver(getHubURL(), options)
              : new ChromeDriver(System.getProperty("TESTPROJECT_DEVELOPER_TOKEN"),options);
    }

    if (listeners != null) {
      listeners.forEach(b -> b.afterInit(logger, webDriver));
    }
    return webDriver;
  }

  @Override
  public CBrowser getBrowser() {
    return CBrowser.CHROME;
  }

  public CTestProjectChromeDriverProvider setBinary(String path) {
    options.setBinary(path);
    return this;
  }

  public CTestProjectChromeDriverProvider addArguments(Iterable<String> args) {
    for (String arg : args) {
      options.addArguments(arg);
    }
    return this;
  }

  public CTestProjectChromeDriverProvider setHeadless(boolean value) {
    options.setHeadless(value);
    addArguments(CChromeConfigs.getHeadLessArguments());
    return this;
  }

  public CTestProjectChromeDriverProvider setDownloadFolder(File tempDownloadFolder) {
    prefs.put("download.default_directory", CFile.of(tempDownloadFolder).getCanonicalPath());
    return this;
  }

  public CTestProjectChromeDriverProvider setOpenPdfInNewTab(boolean value) {
    prefs.put("plugins.always_open_pdf_externally", value);
    return this;
  }

  public CTestProjectChromeDriverProvider setPageLoadStrategy(PageLoadStrategy pageLoadStrategy) {
    options.setCapability(CapabilityType.PAGE_LOAD_STRATEGY, pageLoadStrategy.toString());
    return this;
  }

  public CTestProjectChromeDriverProvider setDeviceEmulation(String deviceName) {
    Map<String, String> mobileEmulation = new HashMap<>();
    mobileEmulation.put("deviceName", deviceName);
    options.setExperimentalOption("mobileEmulation", mobileEmulation);
    return this;
  }

  public CTestProjectChromeDriverProvider addPluginsToDisable(Iterable<String> pluginsToDisable) {
    new CList<>(pluginsToDisable).forEach(p -> addPlugin(false, p));
    return this;
  }

  public CTestProjectChromeDriverProvider addPluginsToEnable(Iterable<String> pluginsToEnable) {
    new CList<>(pluginsToEnable).forEach(p -> addPlugin(true, p));
    return this;
  }

  public CTestProjectChromeDriverProvider addPlugin(boolean flag, String pluginName) {
    CMap<String, Object> plugin = new CHashMap<>();
    plugin.put("enabled", flag);
    plugin.put("name", pluginName);
    plugins.add(plugin);
    return this;
  }

  private ChromeOptions buildChromeOptions() {
    setSystemProperties();
    options.setExperimentalOption("prefs", prefs);

    options.setAcceptInsecureCerts(true);

    options.setCapability(ChromeOptions.CAPABILITY, options);
    options.setCapability("download.directory_upgrade", false);
    options.setCapability("download.prompt_for_download", false);
    return options;
  }

  private void setSystemProperties() {
    java.util.logging.Logger.getLogger("io.testproject.sdk").setLevel(CGridConfigs.getLogLevel());
    java.util.logging.Logger.getLogger("org.openqa.selenium").setLevel(CGridConfigs.getLogLevel());
    System.setProperty("webdriver.chrome.silentOutput", "true");
    if (!CWebDriverManagerConfigs.isEnabled()
        && CStringUtil.isNotBlank(CChromeConfigs.getDriverPath())) {
      String chromeDriverPath = CChromeConfigs.getDriverPath();
      System.setProperty("webdriver.chrome.driver", chromeDriverPath);
    }
  }
}
