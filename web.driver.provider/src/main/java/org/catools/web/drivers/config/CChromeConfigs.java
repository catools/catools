package org.catools.web.drivers.config;

import org.catools.common.collections.CList;
import org.catools.common.utils.CConfigUtil;
import org.openqa.selenium.PageLoadStrategy;

public class CChromeConfigs {

  public static String getDriverPath() {
    return CConfigUtil.getStringOrElse(Configs.WEB_CHROME_DRIVER_PATH, "");
  }

  public static String getBinaryPath() {
    return CConfigUtil.getStringOrElse(Configs.WEB_CHROME_BINARY_PATH, "");
  }

  public static CList<String> getDefaultArguments() {
    CList<String> defaultIfNotSet =
        CList.of(
            "--disable-extensions",
            "--disable-gpu",
            "--disable-plugins",
            "--disable-infobars",
            "--log-level=3",
            "--silent",
            "--disable-dev-shm-usage",
            "--force-device-scale-factor=1");
    return CList.of(
        CConfigUtil.getStringsOrElse(Configs.WEB_CHROME_DEFAULT_ARGUMENTS, ",", defaultIfNotSet));
  }

  public static PageLoadStrategy getPageLoadStrategy() {
    return PageLoadStrategy.valueOf(
        CConfigUtil.getStringOrElse(
            Configs.WEB_CHROME_PAGE_LOAD_STRATEGY, PageLoadStrategy.NORMAL.name()));
  }

  public static CList<String> getPluginsToDisable() {
    return CList.of(
        CConfigUtil.getStringsOrElse(
            Configs.WEB_CHROME_PLUGINS_TO_DISABLE,
            CConfigUtil.getStringOrElse(Configs.WEB_CHROME_PLUGINS_TO_DISABLE_SEPARATOR, ","),
            CList.of("Chrome PDF Viewer", "Chrome Automation Extension")));
  }

  public static CList<String> getPluginsToEnable() {
    return CList.of(
        CConfigUtil.getStringsOrElse(
            Configs.WEB_CHROME_PLUGINS_TO_ENABLE,
            CConfigUtil.getStringOrElse(Configs.WEB_CHROME_PLUGINS_TO_ENABLE_SEPARATOR, ","),
            CList.of()));
  }

  public static boolean isInHeadLessMode() {
    return CConfigUtil.getBooleanOrElse(Configs.WEB_CHROME_HEADLESS_ENABLE, false);
  }

  public static CList<String> getHeadLessArguments() {
    CList<String> defaultIfNotSet =
        CList.of(
            "--no-online",
            "--disable-gpu",
            "--disable-plugins",
            "--no-sandbox",
            "--disable-setuid-sandbox",
            "--disable-dev-shm-usage");
    return CList.of(
        CConfigUtil.getStringsOrElse(Configs.WEB_CHROME_HEADLESS_ARGUMENTS, ",", defaultIfNotSet));
  }

  public static String getChromeMobileEmulationDeviceName() {
    return CConfigUtil.getStringOrElse(Configs.WEB_CHROME_MOBILE_EMULATION_DEVICE_NAME, "");
  }

  public enum Configs {
    WEB_CHROME_DRIVER_PATH,
    WEB_CHROME_BINARY_PATH,
    WEB_CHROME_DEFAULT_ARGUMENTS,
    WEB_CHROME_PAGE_LOAD_STRATEGY,
    WEB_CHROME_PLUGINS_TO_DISABLE,
    WEB_CHROME_PLUGINS_TO_DISABLE_SEPARATOR,
    WEB_CHROME_PLUGINS_TO_ENABLE,
    WEB_CHROME_PLUGINS_TO_ENABLE_SEPARATOR,
    WEB_CHROME_HEADLESS_ENABLE,
    WEB_CHROME_HEADLESS_ARGUMENTS,
    WEB_CHROME_MOBILE_EMULATION_DEVICE_NAME;
  }
}
