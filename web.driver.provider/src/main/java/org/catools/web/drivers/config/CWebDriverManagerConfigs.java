package org.catools.web.drivers.config;

import org.catools.common.utils.CConfigUtil;

public class CWebDriverManagerConfigs {
  public static boolean isEnabled() {
    return CConfigUtil.getBooleanOrElse(Configs.WEB_DRIVER_MANAGER_ENABLED, true);
  }

  public enum Configs {
    WEB_DRIVER_MANAGER_ENABLED
  }
}
