package org.catools.atlassian.etl.jira.translators;

import com.atlassian.jira.rest.client.api.domain.*;
import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.StringUtils;
import org.catools.atlassian.etl.jira.translators.parsers.CEtlJiraParser;
import org.catools.common.collections.CList;
import org.catools.common.collections.CSet;
import org.catools.tms.etl.cache.CEtlCacheManager;
import org.catools.tms.etl.model.*;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;

import java.util.Objects;

import static org.catools.tms.etl.cache.CEtlCacheManager.*;

@UtilityClass
public class CEtlJiraTranslator {
  public static CEtlVersion translateVersion(CEtlProject project, Version version) {
    return new CEtlVersion(
        String.valueOf(version.getId()),
        version.getName(),
        version.getDescription(),
        version.getReleaseDate() == null ? null : version.getReleaseDate().toDate(),
        version.isArchived(),
        version.isReleased(),
        project);
  }

  public static CEtlUser translateUser(BasicUser user) {
    return new CEtlUser(user.getName(), "", user.getDisplayName());
  }

  public static CEtlItems translateIssues(Logger logger, CSet<Issue> issues) {
    return new CEtlItems(issues.mapToSet(i -> translateIssue(logger, i)));
  }

  public static CEtlItem translateIssue(Logger logger, Issue issue) {
    CEtlItemStatusTransitions statusTransitions = new CEtlItemStatusTransitions();
    if (issue.getChangelog() != null) {
      for (ChangelogGroup changelog : issue.getChangelog()) {
        if (changelog.getAuthor() == null) {
          continue;
        }
        CEtlUser byUsername =
            CEtlCacheManager.readUser(logger, translateUser(changelog.getAuthor()));
        for (ChangelogItem statusChangelog :
            new CSet<>(changelog.getItems())
                .getAll(f -> StringUtils.equalsIgnoreCase(f.getField(), "status"))) {
          CEtlStatus from =
              readStatus(
                  logger,
                  new CEtlStatus(statusChangelog.getFrom(), statusChangelog.getFromString()));
          CEtlStatus to =
              readStatus(
                  logger, new CEtlStatus(statusChangelog.getTo(), statusChangelog.getToString()));
          statusTransitions.add(
              new CEtlItemStatusTransition(byUsername, changelog.getCreated().toDate(), from, to));
        }
      }
    }

    CEtlProject project =
        readProject(
            logger,
            new CEtlProject(
                String.valueOf(issue.getProject().getId()), issue.getProject().getName()));
    CEtlItemMetaDatas metaDatas = getIssueMetaData(logger, issue);
    CEtlVersions versions = readVersions(logger, getIssueVersions(issue, project));
    CEtlStatus status =
        readStatus(
            logger,
            new CEtlStatus(String.valueOf(issue.getStatus().getId()), issue.getStatus().getName()));
    CEtlPriority priority =
        readPriority(
            logger,
            new CEtlPriority(
                String.valueOf(Objects.requireNonNull(issue.getPriority()).getId()), issue.getPriority().getName()));
    CEtlItemType type =
        readType(
            logger,
            new CEtlItemType(
                String.valueOf(issue.getIssueType().getId()),
                issue.getIssueType().getName(),
                issue.getIssueType().getDescription(),
                project));

    return new CEtlItem(
        issue.getKey(),
        issue.getSummary(),
        issue.getCreationDate().toDate(),
        issue.getUpdateDate().toDate(),
        project,
        type,
        versions,
        status,
        priority,
        metaDatas,
        statusTransitions);
  }

  private static CEtlItemMetaDatas getIssueMetaData(Logger logger, Issue issue) {
    CEtlItemMetaDatas metaDatas = new CEtlItemMetaDatas();
    metaDatas.add(new CEtlItemMetaData("OrderId", String.valueOf(issue.getId())));
    for (BasicComponent component : issue.getComponents()) {
      metaDatas.add(new CEtlItemMetaData("Component", component.getName()));
    }

    if (issue.getAssignee() != null
        && StringUtils.isNotBlank(issue.getAssignee().getEmailAddress())) {
      metaDatas.add(new CEtlItemMetaData("Assignee", issue.getAssignee().getEmailAddress()));
    }

    if (issue.getLabels() != null) {
      for (String label : issue.getLabels()) {
        metaDatas.add(new CEtlItemMetaData("Label", label));
      }
    }

    if (issue.getFields() != null) {
      CList<IssueField> noneNull = new CSet<>(issue.getFields()).getAll(CEtlJiraTranslator::valueIsNotNull);
      for (IssueField field : noneNull) {
        CEtlJiraParser.parserJiraField(logger, field)
            .forEach((key, val) -> metaDatas.add(new CEtlItemMetaData(key, val)));
      }
    }
    return metaDatas;
  }

  private static boolean valueIsNotNull(IssueField f) {
    return f.getValue() != null
        && f.getValue() != JSONObject.EXPLICIT_NULL
        && f.getValue() != JSONObject.NULL;
  }

  private static CEtlVersions getIssueVersions(Issue issue, CEtlProject project) {
    CEtlVersions versions = new CEtlVersions();
    new CList<>(issue.getFixVersions()).forEach(v -> versions.add(translateVersion(project, v)));
    new CList<>(issue.getAffectedVersions())
        .forEach(v -> versions.add(translateVersion(project, v)));
    return versions;
  }
}
