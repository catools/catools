package org.catools.atlassian.etl.jira;

import com.atlassian.jira.rest.client.api.domain.BasicProject;
import lombok.experimental.UtilityClass;
import org.catools.atlassian.etl.jira.translators.CEtlJiraTranslator;
import org.catools.atlassian.jira.client.CJiraClient;
import org.catools.common.collections.CSet;
import org.catools.common.date.CDate;
import org.catools.tms.etl.dao.CEtlItemDao;
import org.catools.tms.etl.dao.CEtlLastSyncDao;
import org.slf4j.Logger;

import java.util.Date;

@UtilityClass
public class CEtlJiraSyncClient {
  private static final String JIRA = "JIRA";

  public static void syncJira(
      Logger logger,
      CSet<String> projectNamesToSync,
      int parallelInputCount,
      int parallelOutputCount) {
    CSet<BasicProject> projects = CJiraClient.getProjects();
    Date syncStartTime = CDate.now();
    for (BasicProject project : projects.getAll(p -> projectNamesToSync.contains(p.getName()))) {
      Date lastSync = CEtlLastSyncDao.getItemsLastSync(logger, JIRA, project.getName());
      addItems(logger, project, lastSync, parallelInputCount, parallelOutputCount);
      CEtlLastSyncDao.updateProjectLastSync(logger, JIRA, project.getName(), syncStartTime);
    }
  }

  public static void addItems(
      Logger logger,
      BasicProject project,
      Date lastSync,
      int parallelInputCount,
      int parallelOutputCount) {
    Date syncStartTime = CDate.now();

    CJiraClient.search(
        project.getKey(),
        "Test",
        lastSync,
        parallelInputCount,
        parallelOutputCount,
        issues -> CEtlItemDao.mergeItems(logger, CEtlJiraTranslator.translateIssues(logger, issues)));

    CEtlLastSyncDao.updateItemsLastSync(logger, JIRA, project.getName(), syncStartTime);
  }
}
