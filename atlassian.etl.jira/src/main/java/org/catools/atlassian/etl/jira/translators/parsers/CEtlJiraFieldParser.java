package org.catools.atlassian.etl.jira.translators.parsers;

import org.catools.common.collections.CHashMap;
import org.catools.common.collections.CSet;

public interface CEtlJiraFieldParser {
  int rank();

  default boolean isDefaultValue(String value) {
    return new CSet<>("0.0", "-1", "{}", "[]", "None", "N/A", String.valueOf(Long.MAX_VALUE))
        .contains(value);
  }

  boolean isRightParser();

  CHashMap<String, String> getNameValuePairs();
}
