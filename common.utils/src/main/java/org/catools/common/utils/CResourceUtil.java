package org.catools.common.utils;

import lombok.experimental.UtilityClass;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.catools.common.exception.CResourceNotFoundException;

import javax.annotation.Nullable;
import java.io.File;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.nio.file.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiConsumer;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import static org.catools.common.utils.CSystemUtil.getPlatform;

@UtilityClass
public class CResourceUtil {

  public static InputStream getInputStream(String resourceFullName, @Nullable Class clazz) {
    AtomicReference<InputStream> inputStream = new AtomicReference<>();
    perform(resourceFullName, clazz, (resourceName, is) -> inputStream.set(is));
    return inputStream.get();
  }

  public static List<String> readLines(String resourceFullName, @Nullable Class clazz) {
    return Arrays.asList(getString(resourceFullName, clazz).split("\n"));
  }

  public static String getString(String resourceFullName, @Nullable Class clazz) {
    return new String(getByteArray(resourceFullName, clazz));
  }

  public static byte[] getByteArray(String resourceFullName, @Nullable Class clazz) {
    try {
      return CInputStreamUtil.toByteArray(getInputStream(resourceFullName, clazz));
    } catch (Throwable t) {
      throw new CResourceNotFoundException("Failed to read resource " + resourceFullName, t);
    }
  }

  public static List<File> saveToFolder(
      String resourceFullName, @Nullable Class clazz, File targetFolder) {
    List<File> files = new ArrayList<>();
    perform(
        resourceFullName,
        clazz,
        (resourceName, inputStream) -> {
          File destFile = new File(targetFolder, resourceName);
          destFile.getParentFile().mkdirs();
          CFileUtil.writeByteArrayToFile(destFile, CInputStreamUtil.toByteArray(inputStream));
          files.add(destFile);
        });
    return files;
  }

  public static void perform(
      String resourceName, Class clazz, BiConsumer<String, InputStream> consumer) {
    if (StringUtils.isBlank(resourceName)) {
      throw new CResourceNotFoundException("Resource name cannot be null or empty!");
    }

    Pair<Class, ClassLoader> pair = getClassLoader(resourceName, clazz);
    if (pair == null || pair.getValue().getResource(resourceName) == null) {
      throw new CResourceNotFoundException(resourceName + " resource not found!");
    }

    clazz = pair.getKey();
    try {
      URI uri = pair.getValue().getResource(resourceName).toURI();
      if (uri.getScheme().contains("jar")) {
        URL jar = clazz.getProtectionDomain().getCodeSource().getLocation();
        Path path =
            Paths.get(
                StringUtils.substringAfter(
                    jar.toString(), getPlatform().isWindows() ? "file:/" : "file:"));
        try (FileSystem fs = FileSystems.newFileSystem(path, null)) {
          Path resourcePath = fs.getPath(resourceName);
          if (Files.isDirectory(resourcePath)) {
            try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(resourcePath)) {
              for (Path p : directoryStream) {
                InputStream is = clazz.getResourceAsStream(p.toString());
                if (is == null) {
                  is = getResourceAsStream(p.toString(), clazz);
                }
                if (is == null) {
                  throw new CResourceNotFoundException("Failed to read " + p);
                }
                consumer.accept(Path.of(resourceName, p.getFileName().toString()).toString(), is);
              }
            }
          } else {
            try (JarFile jarFile =
                     new JarFile(clazz.getProtectionDomain().getCodeSource().getLocation().getFile())) {
              JarEntry jarEntry = jarFile.getJarEntry(resourceName);
              consumer.accept(
                  jarEntry.getName(), jarFile.getInputStream(jarFile.getEntry(resourceName)));
            }
          }
        }
      } else {
        Path path = Paths.get(uri);
        if (Files.isDirectory(path)) {
          try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(path)) {
            for (Path p : directoryStream) {
              consumer.accept(
                  Path.of(resourceName, p.getFileName().toString()).toString(),
                  FileUtils.openInputStream(p.toFile()));
            }
          }
        } else {
          consumer.accept(path.getFileName().toString(), FileUtils.openInputStream(new File(uri)));
        }
      }
    } catch (Throwable t) {
      throw new CResourceNotFoundException(
          "Unable to read " + resourceName + " resource or read it properly.", t);
    }
  }

  public static URL getResource(String resource, Class clazz) {
    return getClassLoader(resource, clazz).getValue().getResource(resource);
  }

  public static InputStream getResourceAsStream(String resource, Class clazz) {
    return getClassLoader(resource, clazz).getValue().getResourceAsStream(resource);
  }

  public static Pair<Class, ClassLoader> getClassLoader(String resource, Class clazz) {
    final Map<Class, ClassLoader> classLoaders = new HashMap<>();
    if (clazz != null) {
      classLoaders.put(clazz, clazz.getClassLoader());
      classLoaders.put(clazz.getClass(), clazz.getClass().getClassLoader());
    }
    classLoaders.put(CResourceUtil.class, CResourceUtil.class.getClassLoader());
    for (Class c : classLoaders.keySet()) {
      if (c != null
          && classLoaders.get(c) != null
          && classLoaders.get(c).getResourceAsStream(resource) != null) {
        return Pair.of(clazz, classLoaders.get(c));
      }
    }
    return null;
  }
}
