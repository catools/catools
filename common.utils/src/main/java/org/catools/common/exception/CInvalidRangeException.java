package org.catools.common.exception;

public class CInvalidRangeException extends CRuntimeException {

  public CInvalidRangeException(String message) {
    super(message);
  }
}
