package org.catools.reportportal.listeners;

import com.epam.reportportal.testng.BaseTestNGListener;
import com.epam.reportportal.testng.ITestNGService;
import org.catools.reportportal.service.CReportPortalService;
import rp.com.google.common.base.Supplier;
import rp.com.google.common.base.Suppliers;

public class CReportPortalListener extends BaseTestNGListener {
  private static final Supplier<ITestNGService> SERVICE =
      Suppliers.memoize(() -> new CReportPortalService());

  public CReportPortalListener() {
    super(SERVICE.get());
  }
}
