package org.catools.reportportal.configs;

import org.catools.common.config.CConfigs;
import org.catools.common.utils.CStringUtil;

public class CRPConfigs extends CConfigs {
  public static boolean addPackageNameToMethodDescription() {
    return getConfigs().getBooleanOrElse(Configs.RP_ADD_PACKAGE_NAME_TO_METHOD_DESCRIPTION, false);
  }

  public static boolean addClassNameToMethodDescription() {
    return getConfigs().getBooleanOrElse(Configs.RP_ADD_CLASS_NAME_TO_METHOD_DESCRIPTION, true);
  }

  public static boolean isReportPortalEnable() {
    return CStringUtil.isNotBlank(getLaunchId());
  }

  public static String getLaunchId() {
    return System.getProperty("rp.launch.id");
  }

  private enum Configs {
    RP_ADD_PACKAGE_NAME_TO_METHOD_DESCRIPTION,
    RP_ADD_CLASS_NAME_TO_METHOD_DESCRIPTION
  }
}
