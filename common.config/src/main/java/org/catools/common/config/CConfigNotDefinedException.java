package org.catools.common.config;

/** Signals which throws when attempt to use configuration value before define it. */
public class CConfigNotDefinedException extends RuntimeException {
  private static final String HELP =
      "\nTo work with configuration you need to defined it first.\n"
          + "We need all configuration parameters so we can use this information while you are using application in cli mode.\n"
          + "To do so you need to add this information to your default yaml file in following format:\n"
          + "- name: \"String: <Configuration Name>\"\n"
          + "  descriptions: \"String: <Command line prompt to inform use about the propose of config if the value is not set>\"\n"
          + "  defaultValue: \"String: <Default value so we can use it if value is not set, note that we do not prompt user if default value set>\"\n"
          + "  required: <Boolean: Shall we prompt user for this config if it is not set or we can ignore this>\n"
          + "  sensitive: <Boolean: if the configuration information is sensitive and should mask in log>\n";

  public CConfigNotDefinedException(String configName) {
    super("Configuration is not defined: " + configName + HELP);
  }
}
