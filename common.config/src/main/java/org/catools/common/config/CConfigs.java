package org.catools.common.config;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.catools.common.configs.CPathConfigs;
import org.catools.common.utils.CConsoleUtil;
import org.catools.common.utils.CFileUtil;

import java.util.List;

@Log4j2
public class CConfigs {
  private static final CConfigInfoCollection CONFIGS = new CConfigInfoCollection();

  static {
    CConfigsLoader.load();
    if (StringUtils.isNotBlank(CPathConfigs.getOutputPath())) {
      CConsoleUtil.println("cleanup %s", CFileUtil.getCanonicalPath(CPathConfigs.getOutputRoot()));
      if (!CPathConfigs.getOutputRoot().delete()) {
        log.warn("Failed to delete {}.", CPathConfigs.getOutputRoot());
      }
    }
  }

  public static CConfigInfo getByName(String configName) {
    return getConfigs().getByName(configName);
  }

  public static <T extends Enum<T>> CConfigInfo getByName(T config) {
    return getConfigs().getByName(config);
  }

  public static <T extends Enum<T>> boolean getBoolean(T config) {
    return getConfigs().getBoolean(config);
  }

  public static <T extends Enum<T>> boolean getBooleanOrElse(T config, Boolean defaultIfNotSet) {
    return getConfigs().getBooleanOrElse(config, defaultIfNotSet);
  }

  public static <T extends Enum<T>> List<Boolean> getBooleans(T config, String delimiter) {
    return getConfigs().getBooleans(config, delimiter);
  }

  public static <T extends Enum<T>> List<Boolean> getBooleansOrElse(
      T config, String delimiter, List<Boolean> defaultIfNotSet) {
    return getConfigs().getBooleansOrElse(config, delimiter, defaultIfNotSet);
  }

  public static <T extends Enum<T>> double getDouble(T config) {
    return getConfigs().getDouble(config);
  }

  public static <T extends Enum<T>> double getDoubleOrElse(T config, Double defaultIfNotSet) {
    return getConfigs().getDoubleOrElse(config, defaultIfNotSet);
  }

  public static <T extends Enum<T>> int getInteger(T config) {
    return getConfigs().getInteger(config);
  }

  public static <T extends Enum<T>> int getIntegerOrElse(T config, Integer defaultIfNotSet) {
    return getConfigs().getIntegerOrElse(config, defaultIfNotSet);
  }

  public static <T extends Enum<T>> List<Integer> getIntegers(T config, String delimiter) {
    return getConfigs().getIntegers(config, delimiter);
  }

  public static <T extends Enum<T>> List<Integer> getIntegersOrElse(
      T config, String delimiter, List<Integer> defaultIfNotSet) {
    return getConfigs().getIntegersOrElse(config, delimiter, defaultIfNotSet);
  }

  public static <T extends Enum<T>> String getString(T config) {
    return getConfigs().getString(config);
  }

  public static <T extends Enum<T>> String getStringOrElse(T config, String defaultIfNotSet) {
    return getConfigs().getStringOrElse(config, defaultIfNotSet);
  }

  public static <T extends Enum<T>> List<String> getStrings(T config, String delimiter) {
    return getConfigs().getStrings(config, delimiter);
  }

  public static <T extends Enum<T>> List<String> getStringsOrElse(
      T config, String delimiter, List<String> defaultIfNotSet) {
    return getConfigs().getStringsOrElse(config, delimiter, defaultIfNotSet);
  }

  public static CConfigInfoCollection getConfigs() {
    return CONFIGS;
  }
}
