package org.catools.common.config;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.catools.common.configs.CPathConfigs;
import org.catools.common.utils.CConfigUtil;
import org.catools.common.utils.CFileUtil;
import org.catools.common.utils.CResourceUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@UtilityClass
public class CConfigsLoader {
  private static boolean initialized = false;

  // we need this configuration on top and it should send as maven or jvm parameter or should be
  // defined in System properties
  public static final CConfigInfo CONFIGS_TO_LOAD =
      new CConfigInfo()
          .setName("CONFIGS_TO_LOAD")
          .setDescription(
              "Yaml configuration file names to load before issue execution starts (comma separated).")
          .setRequired(true);

  public static void verifyInitialized() {
    if (!initialized) {
      throw new CConfigToLoadNotDefinedException();
    }
  }

  public static synchronized void load() {
    List<String> configsToLoad = new ArrayList<>();
    String configsToLoadValue = CONFIGS_TO_LOAD.getValue();
    if (StringUtils.isBlank(configsToLoadValue)) {
      log.trace(CConfigToLoadNotDefinedException.CONFIG_TO_LOAD_NOT_DEFINED);
      return;
    }

    // Walk through the original list and replace env files with content values
    // to save order we are using a temporary buffer
    String[] tempBuffer = configsToLoadValue.split(",");
    for (String originalValue : tempBuffer) {
      if (originalValue.endsWith(".env")) {
        File file = new File(originalValue);
        if (CResourceUtil.getInputStream(originalValue, CConfigsLoader.class) != null) {
          configsToLoad.addAll(
              CResourceUtil.readLines(originalValue, CConfigsLoader.class).stream()
                  .filter(StringUtils::isNotBlank)
                  .map(String::trim)
                  .collect(Collectors.toList()));
        } else if (file.exists()) {
          configsToLoad.addAll(
              CFileUtil.readLines(file).stream()
                  .filter(StringUtils::isNotBlank)
                  .map(String::trim)
                  .collect(Collectors.toList()));
        } else {
          configsToLoad.add(originalValue);
        }
      } else {
        configsToLoad.add(originalValue);
      }
    }

    configsToLoad.forEach(
        config -> {
          if (CResourceUtil.getInputStream(config, CConfigsLoader.class) != null) {
            loadConfigResource(config);
          } else if (locateFile(config) != null) {
            loadConfigFile(config);
          } else {
            throw new RuntimeException(
                "Cannot locate configuration in resource or local computer. config: " + config);
          }
        });

    // Adding CONFIGS_TO_LOAD to list of configurations
    CConfigs.getConfigs().add(CONFIGS_TO_LOAD);
    CConfigs.getConfigs().forEach(c -> CConfigUtil.setProperty(c.getName(), c.getValue()));
    initialized = true;
  }

  private static void loadConfigFile(String config) {
    List<File> files = new ArrayList<>();
    File cFile = locateFile(config);
    if (cFile != null) {
      if (cFile.isDirectory()) {
        Collections.addAll(files, Objects.requireNonNull(new File(config).listFiles()));
      } else {
        files.add(cFile);
      }
      loadConfigFiles(files);
    } else {
      log.info("No config found for {}", config);
    }
  }

  private static void loadConfigFiles(List<File> files) {
    for (File s : files) {
      CConfigs.getConfigs().mergeYamlFile(locateFile(CFileUtil.getCanonicalPath(s)));
    }
  }

  private static void loadConfigResource(String config) {
    loadConfigFiles(
        CResourceUtil.saveToFolder(
            config, CConfigsLoader.class, new File(System.getProperty("java.io.tmpdir"))));
  }

  protected static File locateFile(String config) {
    File configFile;
    configFile = new File(config);
    if (configFile.exists()) {
      return configFile;
    }

    // If this is first run and configuration is not defined yet then
    // we are not able to define storage and other related folders so we just skip
    try {
      configFile = CPathConfigs.fromStorage(config);
      if (configFile.exists()) {
        return configFile;
      }
    } catch (Throwable t) {
      return null;
    }

    configFile = CPathConfigs.fromOutput(config);
    if (configFile.exists()) {
      return configFile;
    }

    configFile = CPathConfigs.fromTmp(config);
    if (configFile.exists()) {
      return configFile;
    }
    return null;
  }
}
