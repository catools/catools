package org.catools.common.config;

import org.catools.common.utils.CStringUtil;

public class CTestManagementConfigs extends CConfigs {
  public static String getProjectName() {
    return getConfigs().getStringOrElse(Configs.TMS_PROJECT_NAME, "");
  }

  public static String getVersionName() {
    return getConfigs().getStringOrElse(Configs.TMS_VERSION_NAME, "");
  }

  public static String getUrlToTest() {
    return getConfigs().getStringOrElse(Configs.TMS_URL_FORMAT_TO_TEST, "");
  }

  public static String getUrlToTest(String testKey) {
    String string = getUrlToTest();
    return CStringUtil.isBlank(string) ? CStringUtil.EMPTY : CStringUtil.format(string, testKey);
  }

  public static String getUrlToDefect() {
    return getConfigs().getStringOrElse(Configs.TMS_URL_FORMAT_TO_DEFECT, "");
  }

  public static String getUrlToDefect(String testKey) {
    String string = getUrlToDefect();
    return CStringUtil.isBlank(string) ? CStringUtil.EMPTY : CStringUtil.format(string, testKey);
  }

  private enum Configs {
    TMS_URL_FORMAT_TO_DEFECT,
    TMS_URL_FORMAT_TO_TEST,
    TMS_PROJECT_NAME,
    TMS_VERSION_NAME,
  }
}
