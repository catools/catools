package org.catools.common.config;

import org.apache.commons.lang3.StringUtils;
import org.catools.common.utils.CYamlUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

public class CConfigInfoCollection extends HashSet<CConfigInfo> {
  public CConfigInfoCollection() {}

  public <T extends Enum<T>> CConfigInfo getByName(T config) {
    return getByName(config.name());
  }

  public CConfigInfo getByName(String configName) {
    return stream()
        .filter(c -> StringUtils.defaultString(c.getName()).equalsIgnoreCase(configName))
        .findFirst()
        .orElseThrow(() -> new CConfigNotDefinedException(configName));
  }

  public <T extends Enum<T>> boolean getBoolean(T config) {
    String prop = getString(config);
    return !StringUtils.EMPTY.equals(prop) && Boolean.valueOf(prop).booleanValue();
  }

  public <T extends Enum<T>> boolean getBooleanOrElse(T config, boolean defaultIfNotSet) {
    return has(config) ? getBoolean(config) : defaultIfNotSet;
  }

  public <T extends Enum<T>> List<Boolean> getBooleans(T config, String delimiter) {
    return getStrings(config, delimiter).stream()
        .map(Boolean::valueOf)
        .collect(Collectors.toList());
  }

  public <T extends Enum<T>> List<Boolean> getBooleansOrElse(
      T config, String delimiter, List<Boolean> defaultIfNotSet) {
    return has(config) ? getBooleans(config, delimiter) : defaultIfNotSet;
  }

  public <T extends Enum<T>> double getDouble(T config) {
    String prop = getString(config);
    return StringUtils.EMPTY.equals(prop) ? 0 : Double.valueOf(prop).doubleValue();
  }

  public <T extends Enum<T>> double getDoubleOrElse(T config, Double defaultIfNotSet) {
    return has(config) ? getDouble(config) : defaultIfNotSet;
  }

  public <T extends Enum<T>> int getInteger(T config) {
    String prop = getString(config);
    return StringUtils.EMPTY.equals(prop) ? 0 : Integer.valueOf(prop).intValue();
  }

  public <T extends Enum<T>> int getIntegerOrElse(T config, Integer defaultIfNotSet) {
    return has(config) ? getInteger(config) : defaultIfNotSet;
  }

  public <T extends Enum<T>> List<Integer> getIntegers(T config, String delimiter) {
    return getStrings(config, delimiter).stream()
        .map(Integer::valueOf)
        .collect(Collectors.toList());
  }

  public <T extends Enum<T>> List<Integer> getIntegersOrElse(
      T config, String delimiter, List<Integer> defaultIfNotSet) {
    return has(config) ? getIntegers(config, delimiter) : defaultIfNotSet;
  }

  public <T extends Enum<T>> String getString(T config) {
    return getByName(config).getValue();
  }

  public <T extends Enum<T>> String getStringOrElse(T config, String defaultIfNotSet) {
    return has(config) ? getString(config) : defaultIfNotSet;
  }

  public <T extends Enum<T>> List<String> getStrings(T config, String delimiter) {
    String prop = getString(config);
    return StringUtils.isBlank(prop) ? new ArrayList<>() : Arrays.asList(prop.split(delimiter));
  }

  public <T extends Enum<T>> List<String> getStringsOrElse(
      T config, String delimiter, List<String> defaultIfNotSet) {
    return has(config) ? getStrings(config, delimiter) : defaultIfNotSet;
  }

  public <T extends Enum<T>> boolean has(T config) {
    return has(config.name());
  }

  public boolean has(String configName) {
    return stream()
        .filter(c -> StringUtils.defaultString(c.getName()).equalsIgnoreCase(configName))
        .findFirst()
        .isPresent();
  }

  public <T extends Enum<T>> boolean hasNot(T config) {
    return hasNot(config.name());
  }

  public boolean hasNot(String configName) {
    return !has(configName);
  }

  public CConfigInfoCollection merge(CConfigInfoCollection configInfo) {
    configInfo.forEach(
        c -> {
          if (has(c.getName())) {
            getByName(c.getName()).merge(c);
          } else {
            add(c);
          }
        });
    return this;
  }

  public CConfigInfoCollection mergeYamlFile(File file) {
    return merge(CYamlUtil.readFromFile(file, CConfigInfoCollection.class));
  }
}
