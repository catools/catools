package org.catools.common.config;

/** Signals which throws when attempt to work without any configuration. */
public class CConfigToLoadNotDefinedException extends RuntimeException {
  public static final String CONFIG_TO_LOAD_NOT_DEFINED =
      "\nNo configuration found.\n"
          + "We are using CONFIGS_TO_LOAD property to identify which configuration file we should load.\n"
          + "The value(s) of CONFIGS_TO_LOAD should be PATH(s) to resources or file with valid yaml format.\n"
          + "We attempt to find resource with the provided path first.\n"
          + "If no resource found then we try to find a file or directory using provided path\n"
          + "You can set CONFIGS_TO_LOAD value in following ways:\n"
          + "1- If you are using Maven to execute your tests then can either:\n"
          + "\t- Set CONFIGS_TO_LOAD inside your pom file.\n"
          + "\t- Pass CONFIGS_TO_LOAD as Maven command line parameter like -DCONFIGS_TO_LOAD=<value1>,<value2>\n"
          + "2- If you are using cli then pass the parameter in maven format like -DCONFIGS_TO_LOAD=<value1>,<value2>\n"
          + "3- Set the value in System Properties\n"
          + "4- Set the value in Environment Variables\n";

  public CConfigToLoadNotDefinedException() {
    super("Configuration is not defined: CONFIGS_TO_LOAD" + CONFIG_TO_LOAD_NOT_DEFINED);
  }
}
