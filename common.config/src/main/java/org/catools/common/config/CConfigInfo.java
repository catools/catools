package org.catools.common.config;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.StringUtils;
import org.catools.common.utils.CConfigUtil;

@Data
@NoArgsConstructor
@Accessors(chain = true)
public class CConfigInfo {
  private String name;
  private String value;
  private String description;
  private String defaultValue;
  private boolean required;
  private boolean sensitive;

  // We know that we broke some rules here but we need to ensure that at any time
  // if system property or env value changed we read the latest information
  @JsonIgnore
  public String getValue() {
    return isDefined() ? value : CConfigUtil.getValueOrDefault(name, defaultValue);
  }

  @JsonIgnore
  public boolean isDefined() {
    return StringUtils.isNotBlank(value);
  }

  public void merge(CConfigInfo c) {
    setName(c.getName())
        .setValue(c.value)
        .setDefaultValue(c.getDefaultValue())
        .setDescription(c.getDescription())
        .setRequired(c.isRequired())
        .setSensitive(c.isSensitive());
  }
}
