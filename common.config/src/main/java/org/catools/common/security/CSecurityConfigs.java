package org.catools.common.security;

import org.catools.common.config.CConfigInfo;
import org.catools.common.config.CConfigs;

public class CSecurityConfigs extends CConfigs {
  public static void setMaskSensitiveData(boolean flag) {
    if (getConfigs().has(Configs.LOGGER_MASK_SENSITIVE_DATA)) {
      getConfigs().getByName(Configs.LOGGER_MASK_SENSITIVE_DATA).setValue(Boolean.toString(flag));
    } else {
      getConfigs()
          .add(
              new CConfigInfo()
                  .setName(Configs.LOGGER_MASK_SENSITIVE_DATA.name())
                  .setValue(Boolean.toString(flag)));
    }
  }

  public static boolean maskSensitiveData() {
    return getConfigs().getBooleanOrElse(Configs.LOGGER_MASK_SENSITIVE_DATA, true);
  }

  private enum Configs {
    LOGGER_MASK_SENSITIVE_DATA
  }
}
