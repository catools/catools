package org.catools.atlassian.zapi.configs;

import lombok.experimental.UtilityClass;
import org.catools.common.collections.CHashMap;
import org.catools.common.collections.CList;
import org.catools.common.utils.CConfigUtil;

@UtilityClass
public class CZApiConfigs {
  public static class ZApi {
    public static final CHashMap<String, String> statusMap = new CHashMap<>();

    public static String getZApiUri() {
      return CConfigUtil.getString(Configs.ZAPI_HOME);
    }

    public static CHashMap<String, String> getStatusMap() {
      if (!statusMap.isEmpty()) {
        return statusMap;
      }
      for (String[] strings :
          CList.of(CConfigUtil.getStrings(Configs.ZAPI_STATUS_MAP, ","))
              .mapToSet(s -> s.split(":"))) {
        statusMap.put(strings[0].trim(), strings[1].trim());
      }

      // we need to ensure at the beginning that we map everything to avoid issues after execution
      assert statusMap.isNotEmpty();

      return statusMap;
    }

    public static String getUserName() {
      return CConfigUtil.getString(Configs.ZAPI_USERNAME);
    }

    public static String getPassword() {
      return CConfigUtil.getString(Configs.ZAPI_PASSWORD);
    }

    public static String getCycleName() {
      return CConfigUtil.getString(Configs.ZAPI_CYCLE_NAME);
    }

    public static String getDateSplitter() {
      return CConfigUtil.getString(Configs.ZAPI_DATE_SPLITTER);
    }

    public static CList<String> getDateFormats() {
      return CList.of(CConfigUtil.getStrings(Configs.ZAPI_DATE_FORMAT, getDateSplitter()));
    }

    public static int getSearchBufferSize() {
      return CConfigUtil.getInteger(Configs.ZAPI_SEARCH_BUFFER_SIZE);
    }

    public enum Configs {
      ZAPI_HOME,
      ZAPI_USERNAME,
      ZAPI_PASSWORD,
      ZAPI_CYCLE_NAME,
      ZAPI_STATUS_MAP,
      ZAPI_DATE_FORMAT,
      ZAPI_DATE_SPLITTER,
      ZAPI_SEARCH_BUFFER_SIZE
    }
  }
}
