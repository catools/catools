package org.catools.atlassian.jira.configs;

import org.catools.common.config.CConfigs;
import org.catools.common.utils.CStringUtil;

import java.net.URI;
import java.util.List;

public class CJiraConfigs extends CConfigs {
  public static class Jira {
    public static URI getHomeUri() {
      try {
        String string = getConfigs().getString(Configs.JIRA_HOME);
        if (CStringUtil.isBlank(string)) {
          return null;
        }
        return new URI(string);
      } catch (Throwable t) {
        throw new RuntimeException(t);
      }
    }

    public static List<String> getFieldsToRead() {
      return getConfigs().getStrings(Configs.JIRA_FIELDS, "\\|");
    }

    public static String getUserName() {
      return getConfigs().getString(Configs.JIRA_USERNAME);
    }

    public static String getPassword() {
      return getConfigs().getString(Configs.JIRA_PASSWORD);
    }

    public static String getProjectKey() {
      return getConfigs().getString(Configs.JIRA_PROJECT_KEY);
    }

    public static String getVersionName() {
      return getConfigs().getString(Configs.JIRA_VERSION_NAME);
    }

    public static String getDateSplitter() {
      return getConfigs().getString(Configs.JIRA_DATE_SPLITTER);
    }

    public static List<String> getDateFormats() {
      return getConfigs().getStrings(Configs.JIRA_DATE_FORMAT, getDateSplitter());
    }

    public static int getSearchBufferSize() {
      return getConfigs().getInteger(Configs.JIRA_SEARCH_BUFFER_SIZE);
    }

    public enum Configs {
      JIRA_HOME,
      JIRA_FIELDS,
      JIRA_USERNAME,
      JIRA_PASSWORD,
      JIRA_DATE_FORMAT,
      JIRA_PROJECT_KEY,
      JIRA_VERSION_NAME,
      JIRA_DATE_SPLITTER,
      JIRA_SEARCH_BUFFER_SIZE
    }
  }
}
