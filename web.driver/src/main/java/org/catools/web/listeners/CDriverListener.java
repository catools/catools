package org.catools.web.listeners;

import org.catools.web.entities.CWebPageInfo;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;

import java.util.Date;

public interface CDriverListener {
  default void beforeInit(Logger logger, Capabilities capabilities) {}

  default void afterInit(Logger logger, RemoteWebDriver remoteWebDriver) {}

  default void beforeAction(
      Logger logger, RemoteWebDriver webDriver, CWebPageInfo currentPage, String actionName) {}

  default void afterAction(
      Logger logger,
      String actionName,
      RemoteWebDriver webDriver,
      CWebPageInfo pageBeforeAction,
      CWebPageInfo pageAfterAction,
      Date startTime) {}

  default void onPageChanged(
      Logger logger,
      RemoteWebDriver webDriver,
      CWebPageInfo previousPage,
      CWebPageInfo currentPage) {}
}
