package org.catools.web.config;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.catools.common.utils.CConfigUtil;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;

@Log4j2
public class CGridConfigs {

  public static boolean isUseRemoteDriver() {
    return CConfigUtil.getBooleanOrElse(Configs.WEB_GRID_USE_REMOTE_DRIVER, false);
  }

  public static URL getHubURL() {
    try {
      if (!StringUtils.isBlank(getGridHubIP())) {
        return new URL(String.format("http://%s:%s/wd/hub", getGridHubIP(), getGridHubPort()));
      }
    } catch (MalformedURLException e) {
      log.error("Failed to get hub url", e);
    }
    return null;
  }

  public static int getGridHubPort() {
    return CConfigUtil.getIntegerOrElse(Configs.WEB_GRID_HUB_PORT, 4444);
  }

  public static String getGridHubIP() {
    return CConfigUtil.getStringOrElse(Configs.WEB_GRID_HUB_IP, "localhost");
  }

  public static boolean isUseHubFolderModeIsOn() {
    return CConfigUtil.getBooleanOrElse(Configs.WEB_GRID_USE_HUB_FOLDERS, true);
  }

  public static boolean isUseLocalFileDetectorInOn() {
    return CConfigUtil.getBooleanOrElse(Configs.WEB_GRID_USE_LOCAL_FILE_DETECTOR, true);
  }

  public static Level getLogLevel() {
    return Level.parse(CConfigUtil.getStringOrElse(Configs.WEB_GRID_LOG_LEVEL, "OFF"));
  }

  public enum Configs {
    WEB_GRID_LOG_LEVEL,
    WEB_GRID_USE_REMOTE_DRIVER,
    WEB_GRID_HUB_IP,
    WEB_GRID_USE_HUB_FOLDERS,
    WEB_GRID_HUB_PORT,
    WEB_GRID_USE_LOCAL_FILE_DETECTOR
  }
}
