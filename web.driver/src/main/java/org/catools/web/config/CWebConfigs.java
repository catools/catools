package org.catools.web.config;

import org.apache.commons.lang3.StringUtils;
import org.catools.common.configs.CPathConfigs;
import org.catools.common.io.CFile;
import org.catools.common.utils.CConfigUtil;
import org.catools.web.enums.CBrowser;
import org.catools.web.utils.CGridUtil;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;

public class CWebConfigs {

  public static CFile getScreenShotsFolder() {
    return CFile.of(CPathConfigs.getOutputChildFolder("screenshots"));
  }

  public static String getExpectedImagesFolderResourcePath() {
    return CConfigUtil.getStringOrElse(
        Configs.WEB_EXPECTED_IMAGES_RESOURCE_PATH, "/images/expected");
  }

  public static CBrowser getCurrentBrowser() {
    return CBrowser.valueOf(
        CConfigUtil.getStringOrElse(Configs.WEB_DEFAULT_BROWSER, CBrowser.CHROME.name()));
  }

  public static CFile getDownloadFolder(WebDriver driver) {
    CFile downloadedFile;
    if (CGridConfigs.isUseRemoteDriver() && !CGridConfigs.isUseHubFolderModeIsOn()) {
      downloadedFile =
          CFile.fromRemote(
              CGridUtil.getHostNameAndPort(driver)[0], CPathConfigs.getTmpDownloadFolder());
    } else {
      downloadedFile = CFile.of(CPathConfigs.getTmpDownloadFolder());
    }
    if (!downloadedFile.exists()) {
      downloadedFile.mkdirs();
    }
    return downloadedFile;
  }

  /**
   * CDriverConfigs.getTimeout()
   *
   * @return
   */
  @Deprecated
  public static int getTimeout() {
    return CDriverConfigs.getTimeout();
  }

  public static Point getWindowsPosition() {
    String size = CConfigUtil.getStringOrElse(Configs.WEB_BROWSER_WINDOWS_POSITION, "");
    if (StringUtils.isBlank(size)) {
      return null;
    }
    String[] split = size.split(",");
    return new Point(Integer.valueOf(split[0]), Integer.valueOf(split[1]));
  }

  public static Dimension getWindowsDimension() {
    String size = CConfigUtil.getStringOrElse(Configs.WEB_BROWSER_WINDOWS_DIMENSION, "");
    if (StringUtils.isBlank(size)) {
      return null;
    }
    String[] split = size.split(",");
    return new Dimension(Integer.valueOf(split[0]), Integer.valueOf(split[1]));
  }

  public enum Configs {
    WEB_EXPECTED_IMAGES_RESOURCE_PATH,
    WEB_BROWSER_WINDOWS_POSITION,
    WEB_BROWSER_WINDOWS_DIMENSION,
    WEB_DEFAULT_BROWSER,
    WEB_BROWSER_TIMEOUT
  }
}
