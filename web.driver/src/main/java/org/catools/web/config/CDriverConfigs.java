package org.catools.web.config;

import org.catools.common.utils.CConfigUtil;

public class CDriverConfigs {

  public static boolean waitCompleteReadyStateBeforeEachAction() {
    return CConfigUtil.getBooleanOrElse(
        Configs.WEB_DRIVER_WAIT_COMPLETE_READY_STATE_BEFORE_EACH_ACTION, false);
  }

  public static boolean useJS() {
    return CConfigUtil.getBooleanOrElse(Configs.WEB_DRIVER_USE_JAVASCRIPT, false);
  }

  public static int getTimeout() {
    return CConfigUtil.getIntegerOrElse(Configs.WEB_BROWSER_TIMEOUT, 15);
  }

  public enum Configs {
    WEB_DRIVER_WAIT_COMPLETE_READY_STATE_BEFORE_EACH_ACTION,
    WEB_DRIVER_USE_JAVASCRIPT,
    WEB_BROWSER_TIMEOUT
  }
}
