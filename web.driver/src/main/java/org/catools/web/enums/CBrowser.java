package org.catools.web.enums;

public enum CBrowser {
  FIREFOX,
  CHROME,
  EDGE,
  TESTPROJECT_CHROME,
  TESTPROJECT_FIREFOX;

  public boolean isChrome() {
    return CHROME.equals(this);
  }

  public boolean isFirefox() {
    return FIREFOX.equals(this);
  }

  public boolean isEdge() { return EDGE.equals(this); }

  public boolean isTestProjectChrome() { return TESTPROJECT_CHROME.equals(this); }

  public boolean isTestProjectFirefox() { return TESTPROJECT_FIREFOX.equals(this); }
}
