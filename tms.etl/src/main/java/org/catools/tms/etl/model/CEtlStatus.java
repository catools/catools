package org.catools.tms.etl.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.catools.common.utils.CStringUtil;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;

@NamedQueries({@NamedQuery(name = "getStatusByName", query = "FROM CEtlStatus where name=:name")})
@Entity
@Table(name = "status", schema = "etl")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "status")
@Data
@NoArgsConstructor
@Accessors(chain = true)
public class CEtlStatus implements Serializable {

  private static final long serialVersionUID = 6867874018185613707L;

  @Id
  @Column(name = "id", length = 20, unique = true, nullable = false)
  private String id;

  @Column(name = "name", length = 200, unique = true, nullable = false)
  private String name;

  public CEtlStatus(String id, String name) {
    this.id = id;
    this.name = CStringUtil.substring(name, 0, 200);
  }
}
