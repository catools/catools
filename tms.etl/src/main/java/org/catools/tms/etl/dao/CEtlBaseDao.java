package org.catools.tms.etl.dao;

import org.catools.common.collections.interfaces.CCollection;
import org.slf4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;

public class CEtlBaseDao {
  private static EntityManagerFactory entityManagerFactory = null;

  static {
    Runtime.getRuntime()
        .addShutdownHook(
            new Thread(
                () -> {
                  closeEntityManagerFactory();
                }));
  }

  protected static <T> T find(Logger logger, Class<T> entityClass, Object primaryKey) {
    return getTransactionResult(logger, session -> session.find(entityClass, primaryKey));
  }

  protected static <T> void persist(Logger logger, T record) {
    doTransaction(logger, session -> session.persist(record));
  }

  protected static <T> void merge(Logger logger, T record) {
    doTransaction(logger, session -> session.merge(record));
  }

  protected static <T> void persist(Logger logger, CCollection<T> records) {
    logger.trace("Performing bulk persist on {} records.", records.size());
    bulk(logger, records, (session, record) -> session.persist(record));
  }

  protected static <T> void merge(Logger logger, CCollection<T> records) {
    logger.trace("Performing bulk merge on {} records.", records.size());
    bulk(logger, records, (session, record) -> session.merge(record));
  }

  protected static <T> void remove(Logger logger, CCollection<T> records) {
    logger.trace("Performing bulk remove on {} records.", records.size());
    bulk(logger, records, (session, record) -> session.remove(session.merge(record)));
  }

  protected static <T> void bulk(
      Logger logger, CCollection<T> records, BiConsumer<EntityManager, T> action) {
    doTransaction(
        logger,
        session -> {
          int counter = 0;
          for (T record : records) {
            action.accept(session, record);
            if (++counter % 50 == 0) {
              logger.trace(
                  "{} out of {} records processed in this thread.", counter, records.size());
              flashAndClear(session);
            }
          }
          flashAndClear(session);
        });
  }

  protected static <T> T getTransactionResult(Logger logger, Function<EntityManager, T> action) {
    EntityManager session = getEntityManager();
    EntityTransaction tx = null;
    try {
      tx = session.getTransaction();
      tx.begin();
      T result = action.apply(session);
      tx.commit();
      return result;
    } catch (Exception e) {
      if (tx != null) {
        tx.rollback();
      }
      logger.error("Failed To Perform Transaction.", e);
      throw e;
    } finally {
      session.close();
    }
  }

  protected static void doTransaction(Logger logger, Consumer<EntityManager> action) {
    EntityManager session = getEntityManager();
    EntityTransaction tx = null;
    try {
      tx = session.getTransaction();
      tx.begin();
      action.accept(session);
      tx.commit();
    } catch (Exception e) {
      logger.error("Failed To Perform Transaction.", e);
      if (tx != null) {
        tx.rollback();
      }
      throw e;
    } finally {
      session.close();
    }
  }

  protected static synchronized EntityManagerFactory getEntityManagerFactory() {
    if (entityManagerFactory == null) {
      entityManagerFactory = Persistence.createEntityManagerFactory("CEtlPersistence");
    }
    return entityManagerFactory;
  }

  protected static synchronized void closeEntityManagerFactory() {
    if (entityManagerFactory != null && entityManagerFactory.isOpen()) {
      entityManagerFactory.close();
    }
  }

  private static void flashAndClear(EntityManager session) {
    if (session.isOpen()) {
      session.flush();
      session.clear();
    }
  }

  protected static EntityManager getEntityManager() {
    return getEntityManagerFactory().createEntityManager();
  }
}
