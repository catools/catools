package org.catools.tms.etl.dao;

import org.catools.tms.etl.model.CEtlCycle;
import org.catools.tms.etl.model.CEtlCycles;
import org.slf4j.Logger;

public class CEtlCycleDao extends CEtlBaseDao {
  public static void mergeCycle(Logger logger, CEtlCycle cycle) {
    merge(logger, cycle);
  }

  public static void mergeCycles(Logger logger, CEtlCycles cycles) {
    merge(logger, cycles);
  }

  public static CEtlCycle getCycleById(Logger logger, String cycleId) {
    return find(logger, CEtlCycle.class, cycleId);
  }
}
