package org.catools.tms.etl.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.catools.common.utils.CStringUtil;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@NamedQueries({
  @NamedQuery(
      name = "getCycleByProjectAndVersionId",
      query = "FROM CEtlCycle where project_id=:projectid and version_id=:versionid"),
  @NamedQuery(name = "getCycleById", query = "FROM CEtlCycle where id=:id")
})
@Entity
@Table(name = "cycle", schema = "etl")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "cycle")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class CEtlCycle implements Serializable {

  private static final long serialVersionUID = 6051874018185613707L;

  @Id
  @Column(name = "id", length = 20, unique = true, nullable = false)
  private String id;

  @Column(name = "name", length = 250, nullable = false)
  private String name;

  @Column(name = "description", length = 500)
  private String description;

  @Column(name = "environment", length = 250)
  private String environment;

  @Column(name = "end_date")
  private Date endDate;

  @Column(name = "build", length = 250)
  private String build;

  @Column(name = "start_date")
  private Date startDate;

  @ManyToOne(cascade = CascadeType.MERGE, targetEntity = CEtlVersion.class)
  @JoinColumn(
      name = "version_id",
      referencedColumnName = "id",
      nullable = false,
      foreignKey = @ForeignKey(name = "FK_CYCLE_VERSION"))
  private CEtlVersion version;

  public CEtlCycle(
      String id,
      String name,
      String description,
      CEtlVersion version,
      String environment,
      Date endDate,
      String build,
      Date startDate) {
    this.id = id;
    this.name = CStringUtil.substring(name, 0, 250);
    this.description = CStringUtil.substring(description, 0, 500);
    this.version = version;
    this.environment = CStringUtil.substring(environment, 0, 250);
    this.endDate = endDate;
    this.build = CStringUtil.substring(build, 0, 250);
    this.startDate = startDate;
  }
}
