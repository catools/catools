package org.catools.tms.etl.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;

@NamedQueries({@NamedQuery(name = "getUserByName", query = "FROM CEtlUser where name=:name")})
@Entity
@Table(name = "user", schema = "etl")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "user")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class CEtlUser implements Serializable {
  private static final long serialVersionUID = 6267874018185613707L;

  @Id
  @Column(name = "name", length = 100)
  private String name;

  @Column(name = "email", length = 300)
  private String email;

  @Column(name = "display_name", length = 300)
  private String displayName;
}
