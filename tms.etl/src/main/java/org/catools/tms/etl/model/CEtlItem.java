package org.catools.tms.etl.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.catools.common.utils.CStringUtil;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@NamedQueries({
  @NamedQuery(
      name = "getItemByProjectAndVersionId",
      query = "FROM CEtlItem where project_id=:projectid and version_id=:versionid")
})
@Entity
@Table(name = "item", schema = "etl")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "item")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class CEtlItem implements Serializable {

  private static final long serialVersionUID = 6052874018185613707L;

  @Id
  @Column(name = "id", length = 20, unique = true, nullable = false)
  private String id;

  @Column(name = "name", length = 1000, nullable = false)
  private String name;

  @Column(name = "created", nullable = false)
  private Date created;

  @Column(name = "updated")
  private Date updated;

  @ManyToOne(targetEntity = CEtlItemType.class)
  @JoinColumn(name = "type_id", nullable = false, foreignKey = @ForeignKey(name = "FK_ITEM_TYPE"))
  private CEtlItemType type;

  @ManyToOne(targetEntity = CEtlStatus.class)
  @JoinColumn(
      name = "status_id",
      nullable = false,
      foreignKey = @ForeignKey(name = "FK_ITEM_STATUS"))
  private CEtlStatus status;

  @ManyToOne(targetEntity = CEtlPriority.class)
  @JoinColumn(
      name = "priority_id",
      referencedColumnName = "id",
      nullable = false,
      foreignKey = @ForeignKey(name = "FK_ITEM_PRIORITY"))
  private CEtlPriority priority;

  @ManyToOne(targetEntity = CEtlProject.class)
  @JoinColumn(
      name = "project_id",
      referencedColumnName = "id",
      nullable = false,
      foreignKey = @ForeignKey(name = "FK_ITEM_PROJECT"))
  private CEtlProject project;

  @ManyToMany(fetch = FetchType.EAGER, targetEntity = CEtlVersion.class)
  @JoinTable(
      name = "item_version",
      schema = "etl",
      joinColumns = {@JoinColumn(name = "item_id")},
      inverseJoinColumns = {@JoinColumn(name = "version_id")})
  private Set<CEtlVersion> versions = new HashSet<>();

  @OneToMany(
      cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH},
      fetch = FetchType.EAGER,
      targetEntity = CEtlItemMetaData.class)
  @JoinColumn(name = "item_id", referencedColumnName = "id")
  private Set<CEtlItemMetaData> metadata = new HashSet<>();

  @OneToMany(
      cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH},
      fetch = FetchType.EAGER,
      targetEntity = CEtlItemStatusTransition.class)
  @JoinColumn(name = "item_id", referencedColumnName = "id")
  private Set<CEtlItemStatusTransition> statusTransitions = new HashSet<>();

  public CEtlItem(
      String id,
      String name,
      Date created,
      Date updated,
      CEtlProject project,
      CEtlItemType type,
      CEtlVersions versions,
      CEtlStatus status,
      CEtlPriority priority,
      CEtlItemMetaDatas metadata,
      CEtlItemStatusTransitions statusTransitions) {
    this.id = id;
    this.name = CStringUtil.substring(name, 0, 1000);
    this.created = created;
    this.updated = updated;
    this.project = project;
    this.type = type;
    this.versions = versions;
    this.status = status;
    this.priority = priority;
    if (metadata != null && metadata.isNotEmpty()) {
      this.metadata = new HashSet<>(metadata);
    }
    if (statusTransitions != null && statusTransitions.isNotEmpty()) {
      this.statusTransitions = new HashSet<>(statusTransitions);
    }
  }
}
