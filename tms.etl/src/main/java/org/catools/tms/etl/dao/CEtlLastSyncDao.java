package org.catools.tms.etl.dao;

import org.catools.tms.etl.model.CEtlLastSync;
import org.slf4j.Logger;

import java.util.Date;

public class CEtlLastSyncDao extends CEtlBaseDao {
  public static void updateProjectLastSync(
      Logger logger, String component, String projectName, Date syncDate) {
    updateLastSync(logger, String.format("A:%s|P:%s|FullSync", component, projectName), syncDate);
  }

  public static Date getProjectLastSync(Logger logger, String component, String projectName) {
    return getLastSync(logger, String.format("A:%s|P:%s|FullSync", component, projectName));
  }

  public static void updateExecutionLastSync(
      Logger logger, String component, String projectName, String versionName, Date syncDate) {
    updateLastSync(
        logger,
        String.format("A:%s|P:%s|V:%s|Execution", component, projectName, versionName),
        syncDate);
  }

  public static Date getExecutionLastSync(
      Logger logger, String component, String projectName, String versionName) {
    return getLastSync(
        logger, String.format("A:%s|P:%s|V:%s|Execution", component, projectName, versionName));
  }

  public static void updateItemsLastSync(
      Logger logger, String component, String projectName, Date syncDate) {
    updateLastSync(logger, String.format("A:%s|P:%s|Items", component, projectName), syncDate);
  }

  public static Date getItemsLastSync(Logger logger, String component, String projectName) {
    return getLastSync(logger, String.format("A:%s|P:%s|Items", component, projectName));
  }

  private static synchronized void updateLastSync(Logger logger, String key, Date syncDate) {
    merge(logger, new CEtlLastSync(key, syncDate));
  }

  private static Date getLastSync(Logger logger, String key) {
    CEtlLastSync lastSync = find(logger, CEtlLastSync.class, key);
    return lastSync == null ? null : lastSync.getSyncDate();
  }
}
