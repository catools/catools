package org.catools.tms.etl.dao;

import org.catools.tms.etl.model.CEtlPriority;
import org.slf4j.Logger;

public class CEtlPriorityDao extends CEtlBaseDao {
  public static void mergePriority(Logger logger, CEtlPriority priority) {
    merge(logger, priority);
  }

  public static CEtlPriority getPriorityById(Logger logger, String id) {
    return find(logger, CEtlPriority.class, id);
  }
}
