package org.catools.tms.etl.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "status_transition", schema = "etl")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class CEtlItemStatusTransition implements Serializable {

  private static final long serialVersionUID = 6087874018185613707L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @ManyToOne(
      cascade = {CascadeType.ALL},
      targetEntity = CEtlUser.class)
  @JoinColumn(name = "author_id", referencedColumnName = "name")
  private CEtlUser author;

  @Column(name = "occurred")
  private Date occurred;

  @ManyToOne(
      cascade = {CascadeType.ALL},
      targetEntity = CEtlStatus.class)
  @JoinColumn(name = "from_status", referencedColumnName = "id")
  private CEtlStatus from;

  @ManyToOne(
      cascade = {CascadeType.ALL},
      targetEntity = CEtlStatus.class)
  @JoinColumn(name = "to_status", referencedColumnName = "id")
  private CEtlStatus to;

  public CEtlItemStatusTransition(CEtlUser author, Date occurred, CEtlStatus from, CEtlStatus to) {
    this.author = author;
    this.occurred = occurred;
    this.from = from;
    this.to = to;
  }
}
