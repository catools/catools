package org.catools.tms.etl.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.catools.common.utils.CStringUtil;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "metadata", schema = "etl")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class CEtlItemMetaData implements Serializable {

  private static final long serialVersionUID = 6067874018185613707L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "name", length = 250, nullable = false)
  private String name;

  @Column(name = "value", length = 1000)
  private String value;

  public CEtlItemMetaData(String name, String value) {
    this.name = CStringUtil.substring(name, 0, 250);
    this.value = CStringUtil.substring(value, 0, 1000);
  }
}
