package org.catools.tms.etl.dao;

import org.catools.tms.etl.model.CEtlItemType;
import org.slf4j.Logger;

public class CEtlItemTypeDao extends CEtlBaseDao {
  public static void mergeItemType(Logger logger, CEtlItemType itemType) {
    merge(logger, itemType);
  }

  public static CEtlItemType getItemTypeById(Logger logger, String id) {
    return find(logger, CEtlItemType.class, id);
  }
}
