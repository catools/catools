package org.catools.tms.etl.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "priority", schema = "etl")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "priority")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class CEtlPriority implements Serializable {
  private static final long serialVersionUID = 6067874018185613747L;

  @Id
  @Column(name = "id", length = 20)
  private String id;

  @Column(name = "name")
  private String name;
}
