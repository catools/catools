package org.catools.tms.etl.cache;

import org.catools.tms.etl.dao.*;
import org.catools.tms.etl.model.*;
import org.slf4j.Logger;

public class CEtlCacheManager {
  public static synchronized CEtlUser readUser(Logger logger, CEtlUser user) {
    CEtlUser result = CEtlUserDao.getUserByName(logger, user.getName());
    if (result != null) {
      return result;
    }
    CEtlUserDao.mergeUser(logger, user);
    return CEtlUserDao.getUserByName(logger, user.getName());
  }

  public static synchronized CEtlProject readProject(Logger logger, CEtlProject project) {
    CEtlProject result = CEtlProjectDao.getProjectById(logger, project.getId());
    if (result != null) {
      return result;
    }
    CEtlProjectDao.mergeProject(logger, project);
    return CEtlProjectDao.getProjectById(logger, project.getId());
  }

  public static synchronized CEtlVersions readVersions(Logger logger, CEtlVersions versionsToRead) {
    CEtlVersions output = new CEtlVersions();
    for (CEtlVersion version : versionsToRead) {
      output.add(readVersion(logger, version));
    }
    return output;
  }

  public static synchronized CEtlVersion readVersion(Logger logger, CEtlVersion version) {
    CEtlVersion result = CEtlVersionDao.getVersionById(logger, version.getId());
    if (result != null) {
      return result;
    }
    CEtlVersionDao.mergeVersion(logger, version);
    return CEtlVersionDao.getVersionById(logger, version.getId());
  }

  public static synchronized CEtlStatus readStatus(Logger logger, CEtlStatus status) {
    CEtlStatus result = CEtlStatusDao.getStatusById(logger, status.getId());
    if (result != null) {
      return result;
    }
    CEtlStatusDao.mergeStatus(logger, status);
    return CEtlStatusDao.getStatusById(logger, status.getId());
  }

  public static synchronized CEtlExecutionStatus readExecutionStatus(
      Logger logger, CEtlExecutionStatus status) {
    CEtlExecutionStatus result = CEtlExecutionStatusDao.getStatusById(logger, status.getId());
    if (result != null) {
      return result;
    }
    CEtlExecutionStatusDao.mergeStatus(logger, status);
    return CEtlExecutionStatusDao.getStatusById(logger, status.getId());
  }

  public static synchronized CEtlPriority readPriority(Logger logger, CEtlPriority priority) {
    CEtlPriority result = CEtlPriorityDao.getPriorityById(logger, priority.getId());
    if (result != null) {
      return result;
    }
    CEtlPriorityDao.mergePriority(logger, priority);
    return CEtlPriorityDao.getPriorityById(logger, priority.getId());
  }

  public static synchronized CEtlItemType readType(Logger logger, CEtlItemType type) {
    CEtlItemType result = CEtlItemTypeDao.getItemTypeById(logger, type.getId());
    if (result != null) {
      return result;
    }
    CEtlItemTypeDao.mergeItemType(logger, type);
    return CEtlItemTypeDao.getItemTypeById(logger, type.getId());
  }
}
