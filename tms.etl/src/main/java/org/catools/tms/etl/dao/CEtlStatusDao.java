package org.catools.tms.etl.dao;

import org.catools.tms.etl.model.CEtlStatus;
import org.slf4j.Logger;

public class CEtlStatusDao extends CEtlBaseDao {
  public static CEtlStatus getStatusById(Logger logger, String id) {
    return find(logger, CEtlStatus.class, id);
  }

  public static void mergeStatus(Logger logger, CEtlStatus status) {
    merge(logger, status);
  }
}
