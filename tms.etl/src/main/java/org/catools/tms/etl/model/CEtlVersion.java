package org.catools.tms.etl.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.catools.common.utils.CStringUtil;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@NamedQueries({
  @NamedQuery(
      name = "getVersionForProjectAndName",
      query = "FROM CEtlVersion where name=:name and project_id=:projectId")
})
@Entity
@Table(name = "version", schema = "etl")
@Data
@NoArgsConstructor
@Accessors(chain = true)
public class CEtlVersion implements Serializable {

  private static final long serialVersionUID = 6067874018185683707L;

  @Id
  @Column(name = "id", length = 20, unique = true, nullable = false)
  private String id;

  @Column(name = "name", length = 300, nullable = false)
  private String name;

  @Column(name = "description", length = 300)
  private String description;

  @Column(name = "release_date")
  private Date releaseDate;

  @Column(name = "archived")
  private boolean archived;

  @Column(name = "released")
  private boolean released;

  @ManyToOne(targetEntity = CEtlProject.class)
  @JoinColumn(name = "project_id", referencedColumnName = "id", nullable = false)
  private CEtlProject project;

  public CEtlVersion(
      String externalId,
      String name,
      String description,
      Date releaseDate,
      boolean archived,
      boolean released,
      CEtlProject project) {
    this.id = externalId;
    this.name = CStringUtil.substring(name, 0, 300);
    this.description = CStringUtil.substring(description, 0, 300);
    this.releaseDate = releaseDate;
    this.archived = archived;
    this.released = released;
    this.project = project;
  }
}
