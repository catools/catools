package org.catools.tms.etl.dao;

import org.catools.tms.etl.model.CEtlItem;
import org.catools.tms.etl.model.CEtlItems;
import org.slf4j.Logger;

public class CEtlItemDao extends CEtlBaseDao {
  public static void mergeItems(Logger logger, CEtlItems items) {
    merge(logger, items);
  }

  public static CEtlItem getItemById(Logger logger, String itemId) {
    return find(logger, CEtlItem.class, itemId);
  }
}
