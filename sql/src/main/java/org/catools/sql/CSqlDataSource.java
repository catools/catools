package org.catools.sql;

import org.apache.commons.lang3.ArrayUtils;
import org.catools.common.collections.CHashMap;
import org.catools.common.collections.CList;
import org.catools.common.utils.CRetry;
import org.catools.common.utils.CStringUtil;
import org.slf4j.Logger;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.CallableStatementCallback;
import org.springframework.jdbc.core.CallableStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Types;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;

public class CSqlDataSource {
  private static final CHashMap<String, DataSource> dataSourcesMap = new CHashMap<>();

  public static void addDataSource(String sourceName, DataSource dbSource) {
    dataSourcesMap.put(sourceName, dbSource);
  }

  public static class QueryString {
    public static String query(Logger logger, String sql, String dbSource) {
      return query(logger, sql, new MapSqlParameterSource(), dbSource);
    }

    public static String query(
        Logger logger, String sql, MapSqlParameterSource paramSource, String dbSource) {
      return doAction(
          logger,
          "queryForString",
          dbSource,
          sql,
          paramSource,
          jdbcTemplate -> {
            try {
              String result = jdbcTemplate.queryForObject(sql, paramSource, String.class);
              logTrace(logger, "Result: " + result);
              return result;
            } catch (EmptyResultDataAccessException e) {
              return "";
            }
          });
    }

    public static String query(
        Logger logger,
        String sql,
        String dbSource,
        Predicate<String> retryCondition,
        int retryCount,
        int interval) {
      return doWithRetry(
          integer -> query(logger, sql, dbSource), retryCondition, retryCount, interval, null);
    }

    public static String query(
        Logger logger,
        String sql,
        String dbSource,
        Predicate<String> retryCondition,
        int retryCount,
        int interval,
        String orElse) {
      return doWithRetry(
          integer -> query(logger, sql, dbSource), retryCondition, retryCount, interval, orElse);
    }

    public static String query(
        Logger logger,
        String sql,
        MapSqlParameterSource paramSource,
        String dbSource,
        Predicate<String> retryCondition,
        int retryCount,
        int interval) {
      return doWithRetry(
          integer -> query(logger, sql, paramSource, dbSource),
          retryCondition,
          retryCount,
          interval,
          null);
    }

    public static String query(
        Logger logger,
        String sql,
        MapSqlParameterSource paramSource,
        String dbSource,
        Predicate<String> retryCondition,
        int retryCount,
        int interval,
        String orElse) {
      return doWithRetry(
          integer -> query(logger, sql, paramSource, dbSource),
          retryCondition,
          retryCount,
          interval,
          orElse);
    }
  }

  public static class QueryDate {
    public static Date query(Logger logger, String sql, String dbSource) {
      return query(logger, sql, new MapSqlParameterSource(), dbSource);
    }

    public static Date query(
        Logger logger, String sql, MapSqlParameterSource paramSource, String dbSource) {
      return doAction(
          logger,
          "queryForDate",
          dbSource,
          sql,
          paramSource,
          jdbcTemplate -> {
            try {
              Date result = jdbcTemplate.queryForObject(sql, paramSource, Date.class);
              logTrace(logger, "Result: " + result);
              return result;
            } catch (EmptyResultDataAccessException e) {
              return null;
            }
          });
    }

    public static Date query(
        Logger logger,
        String sql,
        String dbSource,
        Predicate<Date> retryCondition,
        int retryCount,
        int interval) {
      return doWithRetry(
          integer -> query(logger, sql, dbSource), retryCondition, retryCount, interval, null);
    }

    public static Date query(
        Logger logger,
        String sql,
        String dbSource,
        Predicate<Date> retryCondition,
        int retryCount,
        int interval,
        Date orElse) {
      return doWithRetry(
          integer -> query(logger, sql, dbSource), retryCondition, retryCount, interval, orElse);
    }

    public static Date query(
        Logger logger,
        String sql,
        MapSqlParameterSource paramSource,
        String dbSource,
        Predicate<Date> retryCondition,
        int retryCount,
        int interval) {
      return doWithRetry(
          integer -> query(logger, sql, paramSource, dbSource),
          retryCondition,
          retryCount,
          interval,
          null);
    }

    public static Date query(
        Logger logger,
        String sql,
        MapSqlParameterSource paramSource,
        String dbSource,
        Predicate<Date> retryCondition,
        int retryCount,
        int interval,
        Date orElse) {
      return doWithRetry(
          integer -> query(logger, sql, paramSource, dbSource),
          retryCondition,
          retryCount,
          interval,
          orElse);
    }
  }

  public static class QueryInt {
    public static Integer query(Logger logger, String sql, String dbSource) {
      return query(logger, sql, new MapSqlParameterSource(), dbSource);
    }

    public static Integer query(
        Logger logger, String sql, MapSqlParameterSource paramSource, String dbSource) {
      return doAction(
          logger,
          "queryForInt",
          dbSource,
          sql,
          paramSource,
          jdbcTemplate -> {
            try {
              int result = jdbcTemplate.queryForObject(sql, paramSource, Integer.class);
              logTrace(logger, "Result: " + result);
              return result;
            } catch (EmptyResultDataAccessException e) {
              return null;
            }
          });
    }

    public static Integer query(
        Logger logger,
        String sql,
        String dbSource,
        Predicate<Integer> retryCondition,
        int retryCount,
        int interval) {
      return doWithRetry(
          integer -> query(logger, sql, dbSource), retryCondition, retryCount, interval, null);
    }

    public static Integer query(
        Logger logger,
        String sql,
        String dbSource,
        Predicate<Integer> retryCondition,
        int retryCount,
        int interval,
        Integer orElse) {
      return doWithRetry(
          integer -> query(logger, sql, dbSource), retryCondition, retryCount, interval, orElse);
    }

    public static Integer query(
        Logger logger,
        String sql,
        MapSqlParameterSource paramSource,
        String dbSource,
        Predicate<Integer> retryCondition,
        int retryCount,
        int interval) {
      return doWithRetry(
          integer -> query(logger, sql, paramSource, dbSource),
          retryCondition,
          retryCount,
          interval,
          null);
    }

    public static Integer query(
        Logger logger,
        String sql,
        MapSqlParameterSource paramSource,
        String dbSource,
        Predicate<Integer> retryCondition,
        int retryCount,
        int interval,
        Integer orElse) {
      return doWithRetry(
          integer -> query(logger, sql, paramSource, dbSource),
          retryCondition,
          retryCount,
          interval,
          orElse);
    }
  }

  public static class QueryLong {
    public static Long query(Logger logger, String sql, String dbSource) {
      return query(logger, sql, new MapSqlParameterSource(), dbSource);
    }

    public static Long query(
        Logger logger, String sql, MapSqlParameterSource paramSource, String dbSource) {
      return doAction(
          logger,
          "queryForLong",
          dbSource,
          sql,
          paramSource,
          jdbcTemplate -> {
            try {
              Long result = jdbcTemplate.queryForObject(sql, paramSource, Long.class);
              logTrace(logger, "Result: " + result);
              return result;
            } catch (EmptyResultDataAccessException e) {
              return null;
            }
          });
    }

    public static Long query(
        Logger logger,
        String sql,
        String dbSource,
        Predicate<Long> retryCondition,
        int retryCount,
        int interval) {
      return doWithRetry(
          integer -> query(logger, sql, dbSource), retryCondition, retryCount, interval, null);
    }

    public static Long query(
        Logger logger,
        String sql,
        String dbSource,
        Predicate<Long> retryCondition,
        int retryCount,
        int interval,
        Long orElse) {
      return doWithRetry(
          integer -> query(logger, sql, dbSource), retryCondition, retryCount, interval, orElse);
    }

    public static Long query(
        Logger logger,
        String sql,
        MapSqlParameterSource paramSource,
        String dbSource,
        Predicate<Long> retryCondition,
        int retryCount,
        int interval) {
      return doWithRetry(
          integer -> query(logger, sql, paramSource, dbSource),
          retryCondition,
          retryCount,
          interval,
          null);
    }

    public static Long query(
        Logger logger,
        String sql,
        MapSqlParameterSource paramSource,
        String dbSource,
        Predicate<Long> retryCondition,
        int retryCount,
        int interval,
        Long orElse) {
      return doWithRetry(
          integer -> query(logger, sql, paramSource, dbSource),
          retryCondition,
          retryCount,
          interval,
          orElse);
    }
  }

  public static class QueryList {
    public static <T> CList<T> query(
        Logger logger, String sql, RowMapper<T> rowMapper, String dbSource) {
      return query(logger, sql, new MapSqlParameterSource(), rowMapper, dbSource);
    }

    public static <T> CList<T> query(
        Logger logger,
        String sql,
        MapSqlParameterSource paramSource,
        RowMapper<T> rowMapper,
        String dbSource) {
      return doAction(
          logger,
          "queryForList",
          dbSource,
          sql,
          paramSource,
          jdbcTemplate -> {
            try {
              List<T> result = jdbcTemplate.query(sql, paramSource, rowMapper);
              logTrace(logger, "Row found: " + result.size());
              return new CList(result);
            } catch (EmptyResultDataAccessException e) {
              return new CList<T>();
            }
          });
    }

    public static <T> CList<T> query(
        Logger logger,
        String sql,
        SqlParameterSource paramSource,
        RowMapper<T> rowMapper,
        String dbSource) {
      return doAction(
          logger,
          "queryForList",
          dbSource,
          sql,
          paramSource,
          jdbcTemplate -> {
            try {
              List<T> result = jdbcTemplate.query(sql, paramSource, rowMapper);
              logTrace(logger, "Row found: " + result.size());
              return new CList(result);
            } catch (EmptyResultDataAccessException e) {
              return new CList<T>();
            }
          });
    }

    public static <T> CList<T> query(
        Logger logger, String sql, Class<T> elementType, String dbSource) {
      return query(logger, sql, new MapSqlParameterSource(), elementType, dbSource);
    }

    public static <T> CList<T> query(
        Logger logger,
        String sql,
        MapSqlParameterSource paramSource,
        Class<T> elementType,
        String dbSource) {
      return doAction(
          logger,
          "queryForList",
          dbSource,
          sql,
          paramSource,
          jdbcTemplate -> {
            try {
              List<T> result = jdbcTemplate.queryForList(sql, paramSource, elementType);
              logTrace(logger, "Row found: " + result.size());
              return new CList(result);
            } catch (EmptyResultDataAccessException e) {
              return new CList<T>();
            }
          });
    }

    public static <T> CList<T> query(
        Logger logger,
        String sql,
        SqlParameterSource paramSource,
        Class<T> elementType,
        String dbSource) {
      return doAction(
          logger,
          "queryForList",
          dbSource,
          sql,
          paramSource,
          jdbcTemplate -> {
            try {
              List<T> result = jdbcTemplate.queryForList(sql, paramSource, elementType);
              logTrace(logger, "Row found: " + result.size());
              return new CList(result);
            } catch (EmptyResultDataAccessException e) {
              return new CList<T>();
            }
          });
    }

    public static CList<Map<String, Object>> query(Logger logger, String sql, String dbSource) {
      return query(logger, sql, new MapSqlParameterSource(), dbSource);
    }

    public static CList<Map<String, Object>> query(
        Logger logger, String sql, MapSqlParameterSource paramSource, String dbSource) {
      return doAction(
          logger,
          "queryForList",
          dbSource,
          sql,
          paramSource,
          jdbcTemplate -> {
            try {
              List<Map<String, Object>> result = jdbcTemplate.queryForList(sql, paramSource);
              logTrace(logger, "Row found: " + result.size());
              return new CList(result);
            } catch (EmptyResultDataAccessException e) {
              return new CList<>();
            }
          });
    }

    public static <T> CList<T> query(
        Logger logger,
        String sql,
        RowMapper<T> rowMapper,
        String dbSource,
        Predicate<CList<T>> retryCondition,
        int retryCount,
        int interval) {
      return doWithRetry(
          integer -> query(logger, sql, rowMapper, dbSource),
          retryCondition,
          retryCount,
          interval,
          null);
    }

    public static <T> CList<T> query(
        Logger logger,
        String sql,
        RowMapper<T> rowMapper,
        String dbSource,
        Predicate<CList<T>> retryCondition,
        int retryCount,
        int interval,
        CList<T> orElse) {
      return doWithRetry(
          integer -> query(logger, sql, rowMapper, dbSource),
          retryCondition,
          retryCount,
          interval,
          orElse);
    }

    public static <T> CList<T> query(
        Logger logger,
        String sql,
        Class<T> elementType,
        String dbSource,
        Predicate<CList<T>> retryCondition,
        int retryCount,
        int interval) {
      return doWithRetry(
          integer -> query(logger, sql, new MapSqlParameterSource(), elementType, dbSource),
          retryCondition,
          retryCount,
          interval,
          null);
    }

    public static <T> CList<T> query(
        Logger logger,
        String sql,
        Class<T> elementType,
        String dbSource,
        Predicate<CList<T>> retryCondition,
        int retryCount,
        int interval,
        CList<T> orElse) {
      return doWithRetry(
          integer -> query(logger, sql, new MapSqlParameterSource(), elementType, dbSource),
          retryCondition,
          retryCount,
          interval,
          orElse);
    }

    public static <T> CList<T> query(
        Logger logger,
        String sql,
        MapSqlParameterSource paramSource,
        RowMapper<T> rowMapper,
        String dbSource,
        Predicate<CList<T>> retryCondition,
        int retryCount,
        int interval) {
      return doWithRetry(
          integer -> query(logger, sql, paramSource, rowMapper, dbSource),
          retryCondition,
          retryCount,
          interval,
          null);
    }

    public static <T> CList<T> query(
        Logger logger,
        String sql,
        MapSqlParameterSource paramSource,
        RowMapper<T> rowMapper,
        String dbSource,
        Predicate<CList<T>> retryCondition,
        int retryCount,
        int interval,
        CList<T> orElse) {
      return doWithRetry(
          integer -> query(logger, sql, paramSource, rowMapper, dbSource),
          retryCondition,
          retryCount,
          interval,
          orElse);
    }

    public static <T> CList<T> query(
        Logger logger,
        String sql,
        MapSqlParameterSource paramSource,
        Class<T> elementType,
        String dbSource,
        Predicate<CList<T>> retryCondition,
        int retryCount,
        int interval) {
      return doWithRetry(
          integer -> query(logger, sql, paramSource, elementType, dbSource),
          retryCondition,
          retryCount,
          interval,
          new CList<>());
    }

    public static <T> CList<T> query(
        Logger logger,
        String sql,
        MapSqlParameterSource paramSource,
        Class<T> elementType,
        String dbSource,
        Predicate<CList<T>> retryCondition,
        int retryCount,
        int interval,
        CList<T> orElse) {
      return doWithRetry(
          integer -> query(logger, sql, paramSource, elementType, dbSource),
          retryCondition,
          retryCount,
          interval,
          orElse);
    }

    public static CList<Map<String, Object>> query(
        Logger logger,
        String sql,
        String dbSource,
        Predicate<CList<Map<String, Object>>> retryCondition,
        int retryCount,
        int interval) {
      return doWithRetry(
          integer -> query(logger, sql, dbSource), retryCondition, retryCount, interval, null);
    }

    public static CList<Map<String, Object>> query(
        Logger logger,
        String sql,
        String dbSource,
        Predicate<CList<Map<String, Object>>> retryCondition,
        int retryCount,
        int interval,
        CList<Map<String, Object>> orElse) {
      return doWithRetry(
          integer -> query(logger, sql, dbSource), retryCondition, retryCount, interval, orElse);
    }

    public static CList<Map<String, Object>> query(
        Logger logger,
        String sql,
        MapSqlParameterSource paramSource,
        String dbSource,
        Predicate<CList<Map<String, Object>>> retryCondition,
        int retryCount,
        int interval) {
      return doWithRetry(
          integer -> query(logger, sql, paramSource, dbSource),
          retryCondition,
          retryCount,
          interval,
          null);
    }

    public static CList<Map<String, Object>> query(
        Logger logger,
        String sql,
        MapSqlParameterSource paramSource,
        String dbSource,
        Predicate<CList<Map<String, Object>>> retryCondition,
        int retryCount,
        int interval,
        CList<Map<String, Object>> orElse) {
      return doWithRetry(
          integer -> query(logger, sql, paramSource, dbSource),
          retryCondition,
          retryCount,
          interval,
          orElse);
    }
  }

  public static class QueryMap {
    public static CHashMap<String, Object> query(Logger logger, String sql, String dbSource) {
      return query(logger, sql, new MapSqlParameterSource(), dbSource);
    }

    public static CHashMap<String, Object> query(
        Logger logger, String sql, MapSqlParameterSource paramSource, String dbSource) {
      return doAction(
          logger,
          "queryForMap",
          dbSource,
          sql,
          paramSource,
          jdbcTemplate -> {
            try {
              Map<String, Object> result = jdbcTemplate.queryForMap(sql, paramSource);
              logTrace(logger, "Row found: " + result.size());
              return new CHashMap<>(result);
            } catch (EmptyResultDataAccessException e) {
              return new CHashMap<>();
            }
          });
    }

    public static CHashMap<String, Object> query(
        Logger logger,
        String sql,
        String dbSource,
        Predicate<CHashMap<String, Object>> retryCondition,
        int retryCount,
        int interval) {
      return doWithRetry(
          integer -> query(logger, sql, dbSource), retryCondition, retryCount, interval, null);
    }

    public static CHashMap<String, Object> query(
        Logger logger,
        String sql,
        String dbSource,
        Predicate<CHashMap<String, Object>> retryCondition,
        int retryCount,
        int interval,
        CHashMap<String, Object> orElse) {
      return doWithRetry(
          integer -> query(logger, sql, dbSource), retryCondition, retryCount, interval, orElse);
    }

    public static CHashMap<String, Object> query(
        Logger logger,
        String sql,
        MapSqlParameterSource paramSource,
        String dbSource,
        Predicate<CHashMap<String, Object>> retryCondition,
        int retryCount,
        int interval) {
      return doWithRetry(
          integer -> query(logger, sql, paramSource, dbSource),
          retryCondition,
          retryCount,
          interval,
          null);
    }

    public static CHashMap<String, Object> query(
        Logger logger,
        String sql,
        MapSqlParameterSource paramSource,
        String dbSource,
        Predicate<CHashMap<String, Object>> retryCondition,
        int retryCount,
        int interval,
        CHashMap<String, Object> orElse) {
      return doWithRetry(
          integer -> query(logger, sql, paramSource, dbSource),
          retryCondition,
          retryCount,
          interval,
          orElse);
    }
  }

  public static class QueryObject {
    public static Object query(Logger logger, String sql, String dbSource) {
      return query(logger, sql, new MapSqlParameterSource(), dbSource);
    }

    public static Object query(
        Logger logger, String sql, MapSqlParameterSource paramSource, String dbSource) {
      return doAction(
          logger,
          "queryForObject",
          dbSource,
          sql,
          paramSource,
          jdbcTemplate -> {
            try {
              return jdbcTemplate.queryForObject(sql, paramSource, Object.class);
            } catch (EmptyResultDataAccessException e) {
              return null;
            }
          });
    }

    public static <T> T query(Logger logger, String sql, RowMapper<T> rowMapper, String dbSource) {
      return query(logger, sql, new MapSqlParameterSource(), rowMapper, dbSource);
    }

    public static <T> T query(
        Logger logger,
        String sql,
        MapSqlParameterSource paramSource,
        RowMapper<T> rowMapper,
        String dbSource) {
      return doAction(
          logger,
          "queryForMap",
          dbSource,
          sql,
          paramSource,
          jdbcTemplate -> {
            try {
              T result = jdbcTemplate.queryForObject(sql, paramSource, rowMapper);
              logTrace(logger, "Value found: " + result.toString());
              return result;
            } catch (EmptyResultDataAccessException e) {
              return null;
            }
          });
    }

    public static <T> T query(
        Logger logger,
        String sql,
        RowMapper<T> rowMapper,
        String dbSource,
        Predicate<T> retryCondition,
        int retryCount,
        int interval) {
      return doWithRetry(
          integer -> query(logger, sql, rowMapper, dbSource),
          retryCondition,
          retryCount,
          interval,
          null);
    }

    public static <T> T query(
        Logger logger,
        String sql,
        RowMapper<T> rowMapper,
        String dbSource,
        Predicate<T> retryCondition,
        int retryCount,
        int interval,
        T orElse) {
      return doWithRetry(
          integer -> query(logger, sql, rowMapper, dbSource),
          retryCondition,
          retryCount,
          interval,
          orElse);
    }

    public static <T> T query(
        Logger logger,
        String sql,
        MapSqlParameterSource paramSource,
        RowMapper<T> rowMapper,
        String dbSource,
        Predicate<T> retryCondition,
        int retryCount,
        int interval) {
      return doWithRetry(
          integer -> query(logger, sql, paramSource, rowMapper, dbSource),
          retryCondition,
          retryCount,
          interval,
          null);
    }

    public static <T> T query(
        Logger logger,
        String sql,
        MapSqlParameterSource paramSource,
        RowMapper<T> rowMapper,
        String dbSource,
        Predicate<T> retryCondition,
        int retryCount,
        int interval,
        T orElse) {
      return doWithRetry(
          integer -> query(logger, sql, paramSource, rowMapper, dbSource),
          retryCondition,
          retryCount,
          interval,
          orElse);
    }
  }

  public static class QueryBlob {
    public static String queryAsString(Logger logger, String sql, String dbSource) {
      return doAction(
          logger,
          "",
          dbSource,
          sql,
          "",
          jdbcTemplate -> {
            try {
              return new String((byte[]) QueryObject.query(logger, sql, dbSource));
            } catch (EmptyResultDataAccessException e) {
              return null;
            }
          });
    }

    public static String queryAsString(
        Logger logger, String sql, MapSqlParameterSource paramSource, String dbSource) {
      return doAction(
          logger,
          "queryForBlobAsString",
          dbSource,
          sql,
          paramSource,
          jdbcTemplate -> {
            try {
              return new String((byte[]) QueryObject.query(logger, sql, paramSource, dbSource));
            } catch (EmptyResultDataAccessException e) {
              return null;
            }
          });
    }

    public static String queryAsString(
        Logger logger,
        String sql,
        String dbSource,
        Predicate<String> retryCondition,
        int retryCount,
        int interval) {
      return doWithRetry(
          integer -> QueryString.query(logger, sql, dbSource),
          retryCondition,
          retryCount,
          interval,
          null);
    }

    public static String queryAsString(
        Logger logger,
        String sql,
        String dbSource,
        Predicate<String> retryCondition,
        int retryCount,
        int interval,
        String orElse) {
      return doWithRetry(
          integer -> QueryString.query(logger, sql, dbSource),
          retryCondition,
          retryCount,
          interval,
          orElse);
    }

    public static String queryAsString(
        Logger logger,
        String sql,
        MapSqlParameterSource paramSource,
        String dbSource,
        Predicate<String> retryCondition,
        int retryCount,
        int interval) {
      return doWithRetry(
          integer -> QueryString.query(logger, sql, paramSource, dbSource),
          retryCondition,
          retryCount,
          interval,
          null);
    }

    public static String queryAsString(
        Logger logger,
        String sql,
        MapSqlParameterSource paramSource,
        String dbSource,
        Predicate<String> retryCondition,
        int retryCount,
        int interval,
        String orElse) {
      return doWithRetry(
          integer -> QueryString.query(logger, sql, paramSource, dbSource),
          retryCondition,
          retryCount,
          interval,
          orElse);
    }
  }

  public static class QueryDouble {
    public static Double query(Logger logger, String sql, String dbSource) {
      return query(logger, sql, new MapSqlParameterSource(), dbSource);
    }

    public static Double query(
        Logger logger, String sql, MapSqlParameterSource paramSource, String dbSource) {
      return doAction(
          logger,
          "queryForDouble",
          dbSource,
          sql,
          paramSource,
          jdbcTemplate -> {
            try {
              Double result = jdbcTemplate.queryForObject(sql, paramSource, Double.class);
              logTrace(logger, "Result: " + result);
              return result;
            } catch (EmptyResultDataAccessException e) {
              return null;
            }
          });
    }

    public static Double query(
        Logger logger,
        String sql,
        String dbSource,
        Predicate<Double> retryCondition,
        int retryCount,
        int interval) {
      return doWithRetry(
          integer -> query(logger, sql, dbSource), retryCondition, retryCount, interval, null);
    }

    public static Double query(
        Logger logger,
        String sql,
        String dbSource,
        Predicate<Double> retryCondition,
        int retryCount,
        int interval,
        Double orElse) {
      return doWithRetry(
          integer -> query(logger, sql, dbSource), retryCondition, retryCount, interval, orElse);
    }

    public static Double query(
        Logger logger,
        String sql,
        MapSqlParameterSource paramSource,
        String dbSource,
        Predicate<Double> retryCondition,
        int retryCount,
        int interval) {
      return doWithRetry(
          integer -> query(logger, sql, paramSource, dbSource),
          retryCondition,
          retryCount,
          interval,
          null);
    }

    public static Double query(
        Logger logger,
        String sql,
        MapSqlParameterSource paramSource,
        String dbSource,
        Predicate<Double> retryCondition,
        int retryCount,
        int interval,
        Double orElse) {
      return doWithRetry(
          integer -> query(logger, sql, paramSource, dbSource),
          retryCondition,
          retryCount,
          interval,
          orElse);
    }
  }

  public static class QueryBigDecimal {
    public static BigDecimal query(Logger logger, String sql, String dbSource) {
      return query(logger, sql, new MapSqlParameterSource(), dbSource);
    }

    public static BigDecimal query(
        Logger logger, String sql, MapSqlParameterSource paramSource, String dbSource) {
      return doAction(
          logger,
          "queryForBigDecimal",
          dbSource,
          sql,
          paramSource,
          jdbcTemplate -> {
            try {
              BigDecimal result = jdbcTemplate.queryForObject(sql, paramSource, BigDecimal.class);
              logTrace(logger, "Result: " + result);
              return result;
            } catch (EmptyResultDataAccessException e) {
              return null;
            }
          });
    }

    public static BigDecimal query(
        Logger logger,
        String sql,
        MapSqlParameterSource paramSource,
        String dbSource,
        Predicate<BigDecimal> retryCondition,
        int retryCount,
        int interval) {
      return doWithRetry(
          integer -> query(logger, sql, paramSource, dbSource),
          retryCondition,
          retryCount,
          interval,
          null);
    }

    public static BigDecimal query(
        Logger logger,
        String sql,
        MapSqlParameterSource paramSource,
        String dbSource,
        Predicate<BigDecimal> retryCondition,
        int retryCount,
        int interval,
        BigDecimal orElse) {
      return doWithRetry(
          integer -> query(logger, sql, paramSource, dbSource),
          retryCondition,
          retryCount,
          interval,
          orElse);
    }
  }

  public static class Batch {
    public static CList<Integer> update(Logger logger, List<String> batches, String dbSource) {
      return update(logger, batches, 500, dbSource);
    }

    public static CList<Integer> update(
        Logger logger, List<String> batches, int partitionSize, String dbSource) {
      return doAction(
          logger,
          "batchUpdate",
          dbSource,
          CStringUtil.join(batches, "\n"),
          "",
          jdbcTemplate -> {
            CList<Integer> output = new CList<>();
            for (CList<String> partition : new CList<>(batches).partition(partitionSize)) {
              output.addAll(
                  Arrays.asList(
                      ArrayUtils.toObject(
                          jdbcTemplate
                              .getJdbcOperations()
                              .batchUpdate(partition.toArray(new String[partition.size()])))));
            }
            return output;
          });
    }

    public static CList<Integer> update(
        Logger logger, String sql, List<MapSqlParameterSource> parameters, String dbSource) {
      return update(logger, sql, parameters, 500, dbSource);
    }

    public static CList<Integer> update(
        Logger logger,
        String sql,
        List<MapSqlParameterSource> parameters,
        int partitionSize,
        String dbSource) {
      List<Map<String, Object>> batchValues = new CList<>(parameters).mapToList(p -> p.getValues());
      return doAction(
          logger,
          "batchUpdate",
          dbSource,
          sql,
          CStringUtil.join(batchValues, "\n"),
          jdbcTemplate -> {
            CList<Integer> output = new CList<>();
            for (CList<MapSqlParameterSource> partition :
                new CList<>(parameters).partition(partitionSize)) {
              output.addAll(
                  Arrays.asList(
                      ArrayUtils.toObject(
                          jdbcTemplate.batchUpdate(
                              sql,
                              partition.toArray(new MapSqlParameterSource[partition.size()])))));
            }
            return output;
          });
    }
  }

  public static class Wait {
    public static CHashMap<String, Object> wait(
        Logger logger,
        CallableStatementCreator statement,
        List<SqlParameter> params,
        String dbSource,
        Predicate<CHashMap<String, Object>> retryCondition,
        int retryCount,
        int interval) {
      return doWithRetry(
          integer -> CSqlDataSource.call(logger, statement, params, dbSource),
          retryCondition,
          retryCount,
          interval,
          null);
    }

    public static CHashMap<String, Object> wait(
        Logger logger,
        CallableStatementCreator statement,
        List<SqlParameter> params,
        String dbSource,
        Predicate<CHashMap<String, Object>> retryCondition,
        int retryCount,
        int interval,
        CHashMap<String, Object> orElse) {
      return doWithRetry(
          integer -> CSqlDataSource.call(logger, statement, params, dbSource),
          retryCondition,
          retryCount,
          interval,
          orElse);
    }

    public static CHashMap<String, Object> wait(
        Logger logger,
        CallableStatementCreator statement,
        String dbSource,
        Predicate<CHashMap<String, Object>> retryCondition,
        int retryCount,
        int interval) {
      return doWithRetry(
          integer -> CSqlDataSource.call(logger, statement, dbSource),
          retryCondition,
          retryCount,
          interval,
          null);
    }

    public static CHashMap<String, Object> wait(
        Logger logger,
        CallableStatementCreator statement,
        String dbSource,
        Predicate<CHashMap<String, Object>> retryCondition,
        int retryCount,
        int interval,
        CHashMap<String, Object> orElse) {
      return doWithRetry(
          integer -> CSqlDataSource.call(logger, statement, dbSource),
          retryCondition,
          retryCount,
          interval,
          orElse);
    }
  }

  public static void query(Logger logger, String sql, String dbSource) {
    doAction(
        logger,
        "query",
        dbSource,
        sql,
        "",
        jdbcTemplate -> {
          jdbcTemplate.getJdbcOperations().execute(sql);
          return "";
        });
  }

  public static void delete(Logger logger, String sql, String dbSource) {
    doAction(
        logger,
        "delete",
        dbSource,
        sql,
        "",
        jdbcTemplate -> {
          jdbcTemplate.getJdbcOperations().execute(sql);
          return "";
        });
  }

  public static int update(Logger logger, String sql, String dbSource) {
    return doAction(
        logger,
        "update",
        dbSource,
        sql,
        "",
        jdbcTemplate -> {
          return jdbcTemplate.update(sql, new MapSqlParameterSource());
        });
  }

  public static int update(
      Logger logger, String sql, MapSqlParameterSource paramSource, String dbSource) {
    return doAction(
        logger,
        "update",
        dbSource,
        sql,
        paramSource,
        jdbcTemplate -> {
          return jdbcTemplate.update(sql, paramSource);
        });
  }

  public static int insert(Logger logger, String sql, String dbSource) {
    return doAction(
        logger,
        "insert",
        dbSource,
        sql,
        "",
        jdbcTemplate -> {
          return jdbcTemplate.update(sql, new MapSqlParameterSource());
        });
  }

  public static int insert(
      Logger logger, String sql, MapSqlParameterSource paramSource, String dbSource) {
    return doAction(
        logger,
        "insert",
        dbSource,
        sql,
        paramSource,
        jdbcTemplate -> {
          return jdbcTemplate.update(sql, paramSource);
        });
  }

  public static void query(
      Logger logger, String sql, MapSqlParameterSource paramSource, String dbSource) {
    doAction(
        logger,
        "query",
        dbSource,
        sql,
        paramSource,
        jdbcTemplate -> {
          jdbcTemplate.queryForObject(sql, paramSource, Object.class);
          return "";
        });
  }

  public static CHashMap<String, Object> call(
      Logger logger,
      CallableStatementCreator statement,
      List<SqlParameter> params,
      String dbSource) {
    return doAction(
        logger,
        "get",
        dbSource,
        statement.toString(),
        params.toString(),
        jdbcTemplate -> {
          try {
            Map<String, Object> result = jdbcTemplate.getJdbcOperations().call(statement, params);
            logTrace(logger, "Result: " + result);
            return new CHashMap<>(result);
          } catch (EmptyResultDataAccessException e) {
            return new CHashMap<>();
          }
        });
  }

  public static CHashMap<String, Object> call(
      Logger logger, CallableStatementCreator statement, String dbSource) {
    return call(logger, statement, new ArrayList<>(), dbSource);
  }

  public static String queryWithReturn(Logger logger, String sql, String dbSource) {
    return doAction(
        logger,
        "queryWithReturn",
        dbSource,
        sql,
        "",
        jdbcTemplate -> {
          String output =
              jdbcTemplate
                  .getJdbcOperations()
                  .execute(
                      connection -> {
                        CallableStatement statementCreator = connection.prepareCall(sql);
                        statementCreator.registerOutParameter(1, Types.VARCHAR);
                        return statementCreator;
                      },
                      (CallableStatementCallback<String>)
                          callablestatement -> {
                            callablestatement.executeUpdate();
                            return (String) callablestatement.getObject(1);
                          });
          logTrace(logger, "query returned value is " + output);
          return output;
        });
  }

  private static <R> R doAction(
      Logger logger,
      String actionName,
      String dbSource,
      String sql,
      SqlParameterSource paramSource,
      Function<NamedParameterJdbcTemplate, R> action) {
    return doAction(
        logger,
        actionName,
        dbSource,
        sql,
        paramSource == null ? "" : paramSource.toString(),
        action);
  }

  private static <R> R doAction(
      Logger logger,
      String actionName,
      String dbSource,
      String sql,
      MapSqlParameterSource parameters,
      Function<NamedParameterJdbcTemplate, R> action) {
    return doAction(
        logger,
        actionName,
        dbSource,
        sql,
        parameters == null ? "" : parameters.getValues().toString(),
        action);
  }

  private static <R> R doAction(
      Logger logger,
      String actionName,
      String dbSource,
      String sql,
      String parameters,
      Function<NamedParameterJdbcTemplate, R> action) {
    if (dataSourcesMap.size() == 0) {
      throw new IndexOutOfBoundsException(
          "No connection available.\nPlease use CSqlDataSource.addDataSource to add new datasource.");
    }

    if (CStringUtil.isNotBlank(parameters)) {
      logTrace(
          logger, actionName + " on " + dbSource + " => " + sql + " with parameters " + parameters);
    } else {
      logTrace(logger, actionName + " on " + dbSource + " => " + sql);
    }
    try {
      return action.apply(new NamedParameterJdbcTemplate(dataSourcesMap.get(dbSource)));
    } catch (Throwable t) {
      if (logger != null) {
        logger.error("Failed to Perform " + actionName + " against " + dbSource, t);
      }
      throw t;
    }
  }

  private static <R> R doWithRetry(
      Function<Integer, R> m, Predicate<R> retryCondition, int retryCount, int interval, R orElse) {
    return CRetry.retryIf(m, retryCondition, retryCount, interval, () -> orElse, true);
  }

  private static void logTrace(Logger logger, String msg) {
    if (logger != null) {
      logger.trace(msg);
    }
  }
}
