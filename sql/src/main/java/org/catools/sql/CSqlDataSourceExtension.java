package org.catools.sql;

import org.catools.common.extensions.types.CDynamicNumberExtension;
import org.catools.common.extensions.types.CDynamicObjectExtension;
import org.catools.common.extensions.types.CDynamicStringExtension;
import org.catools.common.extensions.types.interfaces.CDynamicCollectionExtension;
import org.catools.common.extensions.types.interfaces.CDynamicDateExtension;
import org.catools.common.extensions.types.interfaces.CDynamicMapExtension;
import org.slf4j.Logger;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class CSqlDataSourceExtension {

  public static class String {
    public static CDynamicStringExtension query(
        Logger logger, java.lang.String sql, int waitSec, int interval, java.lang.String dbSource) {
      return query(logger, sql, new MapSqlParameterSource(), waitSec, interval, dbSource);
    }

    public static CDynamicStringExtension query(
        Logger logger,
        java.lang.String sql,
        MapSqlParameterSource paramSource,
        int waitSec,
        int interval,
        java.lang.String dbSource) {
      return new CDynamicStringExtension() {
        @Override
        public java.lang.String get() {
          return CSqlDataSource.QueryString.query(logger, sql, paramSource, dbSource);
        }

        @Override
        public int getDefaultWaitIntervalInMilliSeconds() {
          return interval;
        }

        @Override
        public int getDefaultWaitInSeconds() {
          return waitSec;
        }
      };
    }
  }

  public static class Date {
    public static CDynamicDateExtension query(
        Logger logger, java.lang.String sql, int waitSec, int interval, java.lang.String dbSource) {
      return query(logger, sql, new MapSqlParameterSource(), waitSec, interval, dbSource);
    }

    public static CDynamicDateExtension query(
        Logger logger,
        java.lang.String sql,
        MapSqlParameterSource paramSource,
        int waitSec,
        int interval,
        java.lang.String dbSource) {
      return new CDynamicDateExtension() {
        @Override
        public java.util.Date get() {
          return CSqlDataSource.QueryDate.query(logger, sql, paramSource, dbSource);
        }

        @Override
        public int getDefaultWaitIntervalInMilliSeconds() {
          return interval;
        }

        @Override
        public int getDefaultWaitInSeconds() {
          return waitSec;
        }
      };
    }
  }

  public static class Int {
    public static CDynamicNumberExtension<Integer> query(
        Logger logger, java.lang.String sql, int waitSec, int interval, java.lang.String dbSource) {
      return query(logger, sql, new MapSqlParameterSource(), waitSec, interval, dbSource);
    }

    public static CDynamicNumberExtension<Integer> query(
        Logger logger,
        java.lang.String sql,
        MapSqlParameterSource paramSource,
        int waitSec,
        int interval,
        java.lang.String dbSource) {
      return new CDynamicNumberExtension<>() {
        @Override
        public Integer get() {
          return CSqlDataSource.QueryInt.query(logger, sql, paramSource, dbSource);
        }

        @Override
        public int getDefaultWaitIntervalInMilliSeconds() {
          return interval;
        }

        @Override
        public int getDefaultWaitInSeconds() {
          return waitSec;
        }
      };
    }
  }

  public static class Long {
    public static CDynamicNumberExtension<java.lang.Long> query(
        Logger logger, java.lang.String sql, int waitSec, int interval, java.lang.String dbSource) {
      return query(logger, sql, new MapSqlParameterSource(), waitSec, interval, dbSource);
    }

    public static CDynamicNumberExtension<java.lang.Long> query(
        Logger logger,
        java.lang.String sql,
        MapSqlParameterSource paramSource,
        int waitSec,
        int interval,
        java.lang.String dbSource) {
      return new CDynamicNumberExtension<>() {
        @Override
        public java.lang.Long get() {
          return CSqlDataSource.QueryLong.query(logger, sql, paramSource, dbSource);
        }

        @Override
        public int getDefaultWaitIntervalInMilliSeconds() {
          return interval;
        }

        @Override
        public int getDefaultWaitInSeconds() {
          return waitSec;
        }
      };
    }
  }

  public static class Double {
    public static CDynamicNumberExtension<java.lang.Double> query(
        Logger logger, java.lang.String sql, int waitSec, int interval, java.lang.String dbSource) {
      return query(logger, sql, new MapSqlParameterSource(), waitSec, interval, dbSource);
    }

    public static CDynamicNumberExtension<java.lang.Double> query(
        Logger logger,
        java.lang.String sql,
        MapSqlParameterSource paramSource,
        int waitSec,
        int interval,
        java.lang.String dbSource) {
      return new CDynamicNumberExtension<>() {
        @Override
        public java.lang.Double get() {
          return CSqlDataSource.QueryDouble.query(logger, sql, paramSource, dbSource);
        }

        @Override
        public int getDefaultWaitIntervalInMilliSeconds() {
          return interval;
        }

        @Override
        public int getDefaultWaitInSeconds() {
          return waitSec;
        }
      };
    }
  }

  public static class BigDecimal {
    public static CDynamicNumberExtension<java.math.BigDecimal> query(
        Logger logger, java.lang.String sql, int waitSec, int interval, java.lang.String dbSource) {
      return query(logger, sql, new MapSqlParameterSource(), waitSec, interval, dbSource);
    }

    public static CDynamicNumberExtension<java.math.BigDecimal> query(
        Logger logger,
        java.lang.String sql,
        MapSqlParameterSource paramSource,
        int waitSec,
        int interval,
        java.lang.String dbSource) {
      return new CDynamicNumberExtension<>() {
        @Override
        public java.math.BigDecimal get() {
          return CSqlDataSource.QueryBigDecimal.query(logger, sql, paramSource, dbSource);
        }

        @Override
        public int getDefaultWaitIntervalInMilliSeconds() {
          return interval;
        }

        @Override
        public int getDefaultWaitInSeconds() {
          return waitSec;
        }
      };
    }
  }

  public static class List {
    public static <T> CDynamicCollectionExtension<T> query(
        Logger logger,
        java.lang.String sql,
        RowMapper<T> rowMapper,
        int waitSec,
        int interval,
        java.lang.String dbSource) {
      return query(
          logger, sql, new MapSqlParameterSource(), rowMapper, waitSec, interval, dbSource);
    }

    public static <T> CDynamicCollectionExtension<T> query(
        Logger logger,
        java.lang.String sql,
        SqlParameterSource paramSource,
        RowMapper<T> rowMapper,
        int waitSec,
        int interval,
        java.lang.String dbSource) {
      return new CDynamicCollectionExtension<T>() {
        @Override
        public Iterable<T> get() {
          return CSqlDataSource.QueryList.query(logger, sql, paramSource, rowMapper, dbSource);
        }

        @Override
        public int getDefaultWaitIntervalInMilliSeconds() {
          return interval;
        }

        @Override
        public int getDefaultWaitInSeconds() {
          return waitSec;
        }
      };
    }

    public static <T> CDynamicCollectionExtension<T> query(
        Logger logger,
        java.lang.String sql,
        SqlParameterSource paramSource,
        Class<T> elementType,
        int waitSec,
        int interval,
        java.lang.String dbSource) {
      return new CDynamicCollectionExtension<T>() {
        @Override
        public Iterable<T> get() {
          return CSqlDataSource.QueryList.query(logger, sql, paramSource, elementType, dbSource);
        }

        @Override
        public int getDefaultWaitIntervalInMilliSeconds() {
          return interval;
        }

        @Override
        public int getDefaultWaitInSeconds() {
          return waitSec;
        }
      };
    }
  }

  public static class Map {
    public static CDynamicMapExtension<java.lang.String, java.lang.Object> query(
        Logger logger, java.lang.String sql, int waitSec, int interval, java.lang.String dbSource) {
      return query(logger, sql, new MapSqlParameterSource(), waitSec, interval, dbSource);
    }

    public static CDynamicMapExtension<java.lang.String, java.lang.Object> query(
        Logger logger,
        java.lang.String sql,
        MapSqlParameterSource paramSource,
        int waitSec,
        int interval,
        java.lang.String dbSource) {
      return new CDynamicMapExtension<>() {
        @Override
        public java.util.Map<java.lang.String, java.lang.Object> get() {
          return CSqlDataSource.QueryMap.query(logger, sql, paramSource, dbSource);
        }

        @Override
        public int getDefaultWaitIntervalInMilliSeconds() {
          return interval;
        }

        @Override
        public int getDefaultWaitInSeconds() {
          return waitSec;
        }
      };
    }
  }

  public static class Object {
    public static java.lang.Object query(
        Logger logger, java.lang.String sql, int waitSec, int interval, java.lang.String dbSource) {
      return query(logger, sql, new MapSqlParameterSource(), waitSec, interval, dbSource);
    }

    public static java.lang.Object query(
        Logger logger,
        java.lang.String sql,
        MapSqlParameterSource paramSource,
        int waitSec,
        int interval,
        java.lang.String dbSource) {
      return new CDynamicObjectExtension<>() {
        @Override
        public java.lang.Object get() {
          return CSqlDataSource.QueryObject.query(logger, sql, paramSource, dbSource);
        }

        @Override
        public int getDefaultWaitIntervalInMilliSeconds() {
          return interval;
        }

        @Override
        public int getDefaultWaitInSeconds() {
          return waitSec;
        }
      };
    }

    public static <T> CDynamicObjectExtension<T> query(
        Logger logger,
        java.lang.String sql,
        RowMapper<T> rowMapper,
        int waitSec,
        int interval,
        java.lang.String dbSource) {
      return query(
          logger, sql, new MapSqlParameterSource(), rowMapper, waitSec, interval, dbSource);
    }

    public static <T> CDynamicObjectExtension<T> query(
        Logger logger,
        java.lang.String sql,
        MapSqlParameterSource paramSource,
        RowMapper<T> rowMapper,
        int waitSec,
        int interval,
        java.lang.String dbSource) {
      return new CDynamicObjectExtension<T>() {
        @Override
        public T get() {
          return CSqlDataSource.QueryObject.query(logger, sql, paramSource, rowMapper, dbSource);
        }

        @Override
        public int getDefaultWaitIntervalInMilliSeconds() {
          return interval;
        }

        @Override
        public int getDefaultWaitInSeconds() {
          return waitSec;
        }
      };
    }
  }

  public static class Blob {
    public static CDynamicStringExtension queryAsString(
        Logger logger, java.lang.String sql, int waitSec, int interval, java.lang.String dbSource) {
      return queryAsString(logger, sql, new MapSqlParameterSource(), waitSec, interval, dbSource);
    }

    public static CDynamicStringExtension queryAsString(
        Logger logger,
        java.lang.String sql,
        MapSqlParameterSource paramSource,
        int waitSec,
        int interval,
        java.lang.String dbSource) {
      return new CDynamicStringExtension() {
        @Override
        public java.lang.String get() {
          return CSqlDataSource.QueryBlob.queryAsString(logger, sql, paramSource, dbSource);
        }

        @Override
        public int getDefaultWaitIntervalInMilliSeconds() {
          return interval;
        }

        @Override
        public int getDefaultWaitInSeconds() {
          return waitSec;
        }
      };
    }
  }
}
