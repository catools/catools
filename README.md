# CATools 
Feel free to use libs if that works for you or copy/paste any part of the code you want.

If you have any questions or recommendation I will be more than glad to know about them.
I love new challenges and in this journey, there is no judgment, bad or wrong knowledge so do not hesitate to share your thoughts by skype (akeshmiri) or email (kimiak2000@gmail.com).

- Tools:

    - Java 11 (https://download.java.net/java/GA/jdk11/9/GPL/openjdk-11.0.2_windows-x64_bin.zip)
    - Maven 3.6.3 or later

- Add Lombok and Enable Annotation Processing for your idea
- put following line in your testNG Run/Debug configuration for VM parameter (we need this for JMockit)

```
    -ea -javaagent:/root/.m2/repository/org/jmockit/jmockit/1.44/jmockit-1.44.jar
```

#GPG
Follow [gpg_signed_commits](https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/)


##Send Key To Server
```
gpg2 --send-keys --keyserver hkps://keyserver.ubuntu.com XXXXXX
```

##Receive Key To Server
```
gpg2 --keyserver hkps://keyserver.ubuntu.com --receive-key XXXXXX
```

##Export Keys
```
gpg --output pubkey.gpg --armor --export XXXXXX
gpg --output seckey.gpg --armor --export-secret-key XXXXXX
```

##Import Keys
```
gpg --import pub
gpg --allow-secret-key-import --import key

```
