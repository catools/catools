package org.catools.common.date;

import org.catools.common.utils.CConfigUtil;
import org.catools.common.utils.CStringUtil;

import java.util.TimeZone;

public class CDateConfigs {

  public static TimeZone getDefaultTimeZone() {
    String val = CConfigUtil.getStringOrElse(Configs.DATE_TIME_ZONE.name(), "");
    return CStringUtil.isBlank(val) ? TimeZone.getDefault() : TimeZone.getTimeZone(val);
  }

  private enum Configs {
    DATE_TIME_ZONE,
  }
}
