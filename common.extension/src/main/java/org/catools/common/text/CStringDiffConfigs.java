package org.catools.common.text;

import org.catools.common.utils.CConfigUtil;
import org.fusesource.jansi.Ansi;

public class CStringDiffConfigs {
  public static int getDiffEditCost() {
    return CConfigUtil.getIntegerOrElse(Configs.DIFF_TEXT_EDIT_COST.name(), 2);
  }

  public static Ansi.Color getInsertColor() {
    return Ansi.Color.valueOf(
        CConfigUtil.getStringOrElse(Configs.DIFF_TEXT_INSERT_COLOR.name(), "YELLOW"));
  }

  public static Ansi.Color getDeleteColor() {
    return Ansi.Color.valueOf(
        CConfigUtil.getStringOrElse(Configs.DIFF_TEXT_DELETE_COLOR.name(), "RED"));
  }

  public static Ansi.Color getEqualColor() {
    return Ansi.Color.valueOf(
        CConfigUtil.getStringOrElse(Configs.DIFF_TEXT_EQUAL_COLOR.name(), "DEFAULT"));
  }

  public static String getInsertFormat() {
    return CConfigUtil.getStringOrElse(Configs.DIFF_TEXT_INSERT_FORMAT.name(), "|(+)%s|");
  }

  public static String getDeleteFormat() {
    return CConfigUtil.getStringOrElse(Configs.DIFF_TEXT_DELETE_FORMAT.name(), "|(-)%s|");
  }

  public static String getEqualFormat() {
    return CConfigUtil.getStringOrElse(Configs.DIFF_TEXT_EQUAL_FORMAT.name(), "%s");
  }

  private enum Configs {
    DIFF_TEXT_EDIT_COST,
    DIFF_TEXT_INSERT_FORMAT,
    DIFF_TEXT_DELETE_FORMAT,
    DIFF_TEXT_EQUAL_FORMAT,
    DIFF_TEXT_INSERT_COLOR,
    DIFF_TEXT_DELETE_COLOR,
    DIFF_TEXT_EQUAL_COLOR
  }
}
