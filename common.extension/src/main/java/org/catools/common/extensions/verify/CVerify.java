package org.catools.common.extensions.verify;

import org.catools.common.configs.CAnsiConfigs;
import org.catools.common.utils.CAnsiUtil;
import org.slf4j.Logger;

import java.util.function.Function;

public class CVerify extends CVerificationBuilder<CVerify> {

  public CVerify() {
    super();
  }

  public CVerify(Logger logger) {
    super(logger);
  }

  @Override
  public CVerify queue(CVerificationInfo expectation) {
    perform(messages -> expectation.test(logger, messages));
    return this;
  }

  private void perform(Function<StringBuilder, Boolean> supplier) {
    StringBuilder messages =
        new StringBuilder(CAnsiConfigs.isPrintInColorAvailable() ? CAnsiUtil.RESET : "");
    boolean result = supplier.apply(messages);
    String verificationMessages = messages.toString();
    if (!result) {
      logger.error(verificationMessages);
      throw new AssertionError(verificationMessages);
    }
    logger.info(verificationMessages);
  }
}
