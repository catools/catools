package org.catools.common.tests.verify.retry;

import org.catools.common.extensions.verify.CBooleanVerification;
import org.catools.common.extensions.verify.CVerifier;

import java.util.function.Consumer;

public class BooleanVerificationVerifierTest extends BooleanVerificationBaseTest {
  @Override
  public void verify(Consumer<CBooleanVerification> action) {
    CVerifier verifier = new CVerifier(logger);
    action.accept(verifier.Bool);
    verifier.verify();
  }
}
